package leetcode;

/*Given a binary tree, flatten it to a linked list in-place.
 idea: do not forget set left child to null. iterative: preorder using DFS
 For example, Given
 1
 / \
 2   5
 / \   \
 3   4   6
 The flattened tree should look like: 
 1
 \
 2
 \
 3
 \
 4
 \
 5
 \
 6
 /*
 * Input tree:
 *     1
 *    / \
 *   2 
 * output:
 *        1
 *     /     \
 *  #(null)   2 
 *  
 * */
public class FlattenBinaryTreetoLinkedList {

	public void flatten(TreeNode root) {
		if (root == null)
			return;
		TreeNode curr = root;
		TreeNode left = curr.left;
		TreeNode right = curr.right;
		if (left != null) {
			curr.left = null;
			flatten(left);
		}
		if (right != null)
			flatten(right);
		curr.right = left;
		while (curr.right != null)
			curr = curr.right;
		curr.right = right;
	}
}
