package leetcode;

/*Given a binary tree, find its minimum depth.
 The minimum depth is the number of nodes along the shortest path from 
 the root node down to the nearest leaf node.*/
import java.util.ArrayList;

public class MinimumDepthOfBinaryTree {

	// Recursion
	public int minDepth_Recursion(TreeNode root) {
		if (root == null) // this is counted as leaf node
			return 0;
		if (root.left == null)
			return 1 + minDepth_Recursion(root.right);
		if (root.right == null)
			return 1 + minDepth_Recursion(root.left);
		return 1 + Math.min(minDepth_Recursion(root.left),
				minDepth_Recursion(root.right));
	}

	// Iteration
	public int minDepth_Iteration(TreeNode root) {
		if (root == null)
			return 0;
		int depth = 1;
		ArrayList<TreeNode> prev = new ArrayList<TreeNode>();
		prev.add(root);
		while (!prev.isEmpty()) {
			ArrayList<TreeNode> temp = new ArrayList<TreeNode>();
			for (TreeNode node : prev) {
				if (node.left == null && node.right == null)
					return depth;
				if (node.left != null)
					temp.add(node.left);
				if (node.right != null)
					temp.add(node.right);
			}
			prev = new ArrayList<TreeNode>(temp);
			depth++;
		}
		return depth;
	}
}
