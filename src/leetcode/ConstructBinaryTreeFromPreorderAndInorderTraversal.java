package leetcode;

/*Given preorder and inorder traversal of a tree, construct the binary tree.
 Note: You may assume that duplicates do not exist in the tree.
 * pre-order: {1, 2, 4, 5, 3}
 * in-order: {4, 2, 5,1, 3}
 * tree:
 *      1
 *     / \
 *    2   3
 *   / \  
 *  4   5
 */
import java.util.HashMap;

public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
	
	public TreeNode buildTree(int[] preorder, int[] inorder) {
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		for (int i = 0; i < inorder.length; i++)
			map.put(inorder[i], i);
		return buildTree(map, preorder, inorder, 0, preorder.length - 1, 0,
				inorder.length - 1);
	}

	public TreeNode buildTree(HashMap<Integer, Integer> map, int[] preorder,
			int[] inorder, int a, int b, int c, int d) {
		if (b < a || d < c)
			return null;
		TreeNode root = new TreeNode(preorder[a]);
		int i = map.get(preorder[a]);
		root.left = buildTree(map, preorder, inorder, a + 1, a + (i - c), c,
				i - 1);
		root.right = buildTree(map, preorder, inorder, b - (d - i - 1), b,
				i + 1, d);
		return root;
	}
}
