package leetcode;

/* The count-and-say sequence is the sequence of integers beginning as follows:
 1, 11, 21, 1211, 111221, ...
 1 is read off as "one 1" or 11.
 11 is read off as "two 1s" or 21.
 21 is read off as "one 2, then one 1" or 1211.
 Given an integer n, generate the nth sequence.
 Note: The sequence of integers will be represented as a string. */

public class CountAndSay {

	public String countAndSay(int n) {
		String res = String.valueOf(1);
		for (int i = 1; i < n; i++)
			res = cs(res);
		return res;
	}

	public String cs(String s) {
		StringBuilder sb = new StringBuilder();
		int count = 1;
		char ch = s.charAt(0);
		for (int i = 1; i < s.length(); i++) {
			if (ch == s.charAt(i))
				count++;
			else {
				sb.append(count);
				sb.append(ch);
				count = 1;
				ch = s.charAt(i);
			}
		}
		sb.append(count);
		sb.append(ch);
		return sb.toString();
	}
}
