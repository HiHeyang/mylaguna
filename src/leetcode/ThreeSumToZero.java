package leetcode;

/*Given an array S of n integers, are there elements a, b, c in S such that a + b + c = 0? 
 Find all unique triplets in the array which gives the sum of zero.*/
import java.util.ArrayList;

public class ThreeSumToZero {

	public ArrayList<ArrayList<Integer>> threeSum(int[] num) {
		ArrayList<ArrayList<Integer>> result = new ArrayList<ArrayList<Integer>>();
		if (num == null || num.length < 3)
			return result;
		java.util.Arrays.sort(num);
		for (int i = 0; i < num.length - 2; i++) {
			if (i == 0 || num[i] > num[i - 1]) {
				int begin = i + 1;
				int end = num.length - 1;
				while (end > begin) {
					int sum = num[i] + num[begin] + num[end];
					if (sum == 0) {
						ArrayList<Integer> list = new ArrayList<Integer>();
						list.add(num[i]);
						list.add(num[begin]);
						list.add(num[end]);
						result.add(list);
						begin++;
						end--;
						while (end > begin && (num[begin] == num[begin - 1]))
							begin++;
						while (end > begin && (num[end] == num[end + 1]))
							end--;
					} else if (sum < 0)
						begin++;
					else
						end--;
				}
			}
		}
		return result;
	}
}
