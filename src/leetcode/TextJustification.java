package leetcode;

/*Given an array of words and a length L, format the text such that each line has exactly L characters 
 * and is fully (left and right) justified.
 You should pack your words in a greedy approach; that is, pack as many words as you can in each line. 
 Pad extra spaces' ' when necessary so that each line has exactly L characters.
 Extra spaces between words should be distributed as evenly as possible. If the number of spaces on a line 
 do not divide evenly between words, the empty slots on the left will be assigned more spaces than the slots on the right.
 For the last line of text, it should be left justified and no extra space is inserted between words.
 For example, words: ["This", "is", "an", "example", "of", "text", "justification."] L: 16.
 Return the formatted lines as:
 [
 "This    is    an",
 "example  of text",
 "justification.  "
 ]
 Note: Each word is guaranteed not to exceed L in length.*/
import java.util.ArrayList;

public class TextJustification {
	
	public static ArrayList<String> fullJustify(String[] words, int L) {
		ArrayList<String> list = new ArrayList<String>();
		int i = 0;
		while (i < words.length) {
			int size = 0;
			int start = i;
			while (i < words.length) {
				int newSize = size == 0 ? words[i].length() : size + 1
						+ words[i].length();
				if (newSize <= L)
					size = newSize;
				else
					break;
				i++;
			}
			int spaceCount = L - size;
			int everyCount;
			if (i - 1 - start != 0 && i != words.length) {
				everyCount = spaceCount / (i - 1 - start);
				spaceCount %= (i - 1 - start);
			} else
				everyCount = 0;
			String s = "";
			for (int j = start; j < i; j++) {
				if (j == start)
					s = words[j];
				else {
					s += " ";
					for (int k = 0; k < everyCount; k++)
						s += " ";
					if (spaceCount > 0 && i != words.length) {
						s += " ";
						spaceCount--;
					}
					s += words[j];
				}
			}
			for (int p = 0; p < spaceCount; p++)
				s += " ";
			list.add(s);
		}
		return list;
	}
}
