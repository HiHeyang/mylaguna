package leetcode;

/*Given two binary trees, write a function to check if they are equal or not.
 Two binary trees are considered equal if they are structurally identical 
 and the nodes have the same value.*/
import java.util.LinkedList;

public class SameTree {

	// recursion
	public boolean isSameTree_Recursion(TreeNode p, TreeNode q) {
		if (p == null && q == null)
			return true;
		if (p == null && q != null || p != null && q == null)
			return false;
		if (p != null && q != null) {
			if (p.val != q.val)
				return false;
			else
				return isSameTree_Recursion(p.left, q.left)
						&& isSameTree_Recursion(q.right, p.right);
		}
		return false;
	}

	// Iteration
	public boolean isSameTree_Iteration(TreeNode p, TreeNode q) {
		LinkedList<TreeNode> t1 = new LinkedList<TreeNode>(), t2 = new LinkedList<TreeNode>();
		t1.add(p);
		t2.add(q);
		while (!t1.isEmpty() && !t2.isEmpty()) {
			TreeNode temp1 = t1.poll();
			TreeNode temp2 = t2.poll();
			if (temp1 == null && temp2 != null || temp1 != null
					&& temp2 == null)
				return false;
			if (temp1 != null) {
				if (temp1.val != temp2.val)
					return false;
				t1.add(temp1.left);
				t1.add(temp1.right);
				t2.add(temp2.left);
				t2.add(temp2.right);
			}
		}
		return true;
	}
}
