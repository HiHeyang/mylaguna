package leetcode;

/* Given a sorted linked list, delete all nodes that have duplicate numbers, 
 leaving only distinct numbers from the original list.
 For example,
 Given 1->2->3->3->4->4->5, return 1->2->5.
 Given 1->1->1->2->3, return 2->3. */
public class RemoveDuplicatesFromSortedListII {

	public ListNode deleteDuplicates(ListNode head) {
		ListNode curr = head, dumbHead = new ListNode(0), now = dumbHead;
		while (curr != null) {
			boolean isDup = false;
			while (curr.next != null && curr.next.val == curr.val) {
				isDup = true;
				curr = curr.next;
			}
			if (!isDup) {
				now.next = curr;
				now = now.next;
			}
			curr = curr.next;
		}
		now.next = null;
		return dumbHead.next;
	}
}
