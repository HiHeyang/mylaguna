package leetcode;

/*You are climbing a stair case. It takes n steps to reach to the top.
 Each time you can either climb 1 or 2 steps. 
 In how many distinct ways can you climb to the top?
 when i >=3, last step should be one or two steps,if one step,then should be total number of (i-1)steps
 if two steps, then should be total number of (i-2)steps, so i steps total should be add sum*/
public class ClimbStairs {

	public int climbStairs(int n) {
		if (n <= 2)
			return n;
		int prevprev = 1;
		int prev = 2;
		for (int i = 3; i <= n; i++) {
			int ret = prevprev + prev;
			prevprev = prev;
			prev = ret;
		}
		return prev;
	}
}
