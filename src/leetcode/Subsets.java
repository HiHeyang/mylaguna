package leetcode;

/*Given a set of distinct integers, S, return all possible subsets.
 Note:
 Elements in a subset must be in non-descending order.
 The solution set must not contain duplicate subsets.
 For example,
 If S = [1,2,3], a solution is:
 [
 [3],
 [1],
 [2],
 [1,2,3],
 [1,3],
 [2,3],
 [1,2],
 []
 ]*/
import java.util.*;

public class Subsets {

	public List<List<Integer>> subsets(int[] S) {
		if (S == null)
			return null;
		Arrays.sort(S);
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<List<Integer>> cur = new ArrayList<List<Integer>>();
		for (int i = 0; i < S.length; i++) {
			cur = new ArrayList<List<Integer>>();
			for (List<Integer> temp : res)
				cur.add(new ArrayList<Integer>(temp));
			for (List<Integer> temp : cur)
				temp.add(S[i]);
			List<Integer> temp1 = new ArrayList<Integer>();
			temp1.add(S[i]);
			cur.add(temp1);
			res.addAll(cur);
		}
		res.add(new ArrayList<Integer>());
		return res;
	}
}
