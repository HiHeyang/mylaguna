package leetcode;

/*Given a string containing only digits, restore it by returning all possible valid IP address combinations.
 For example: Given "25525511135",
 return ["255.255.11.135", "255.255.111.35"]. (Order does not matter)*/
import java.util.ArrayList;
import java.util.List;

public class RestoreIPAddresses {

	public List<String> restoreIpAddresses(String s) {
		List<String> result = new ArrayList<String>();
		if (s == null || s.length() > 12 || s.length() < 4)
			return result;
		ArrayList<String> cur = new ArrayList<String>();
		dfs(s, cur, result, 0, 0);
		return result;
	}

	public void dfs(String s, ArrayList<String> cur, List<String> res,
			int count, int index) {
		if (count == 4 && index == s.length()) {
			StringBuilder sb = new StringBuilder();
			for (int j = 0; j < 3; j++)
				sb.append(cur.get(j) + ".");
			sb.append(cur.get(3));
			res.add(sb.toString());
			return;
		}
		for (int i = index + 1; i <= s.length() && i < (index + 4); i++) {
			String newstr = s.substring(index, i);
			if (isValid(newstr)) {
				cur.add(newstr);
				dfs(s, cur, res, count + 1, i);
				cur.remove(cur.size() - 1);
			}
		}
	}

	public boolean isValid(String s) {
		if (s.charAt(0) == '0')
			return s.equals("0");
		int num = Integer.parseInt(s);
		return num <= 255 && num > 0;
	}
}
