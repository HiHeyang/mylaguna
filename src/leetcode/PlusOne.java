package leetcode;

//Given a number represented as an array of digits, plus one to the number.
public class PlusOne {

	public int[] plusOne(int[] digits) {
		int len = digits.length;
		int carry = 1;
		for (int i = len - 1; i >= 0; i--) {
			int digit = digits[i] + carry;
			digits[i] = digit % 10;
			carry = digit / 10;
			if (carry == 0)
				return digits;
		}
		int[] result = new int[len + 1];
		result[0] = 1;
		for (int i = 0; i < len; i++)
			result[i + 1] = digits[i];
		return result;
	}
}
