package leetcode;

/*Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.
 If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).
 The replacement must be in-place, do not allocate extra memory.
 Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column. 
 1,2,3 -> 1,3,2 3,2,1 -> 1,2,3 1,1,5 -> 1,5,1
Helper method: reverse().
1. find largest m so that A[m]<A[m+1] (if no m, then reverse whole array and return) and largest n so that A[n]>A[m].
2. swap A[m] and A[n].
3. reverse A[m+1] to end.*/
public class NextPermutation {

	public void nextPermutation(int[] num) {
		int size = num.length;
		int m = -1, n = 0;
		for (int i = 0; i < size - 1; i++) {
			if (num[i] < num[i + 1])
				m = i;
		}
		if (m == -1) {
			reverse(num, 0, size - 1);
			return;
		}
		for (int j = 0; j < size; j++) {
			if (num[j] > num[m])
				n = j;
		}
		int tmp = num[n];
		num[n] = num[m];
		num[m] = tmp;
		reverse(num, m + 1, size - 1);
	}

	public void reverse(int[] num, int start, int end) {
		while (end > start) {
			int tmp = num[end];
			num[end] = num[start];
			num[start] = tmp;
			start++;
			end--;
		}
	}
}
