package leetcode;

//Implement power(x, n).
public class Pow {

	public static double pow(double x, int n) {
		double res = 1.0;
		boolean is_neg = n < 0 ? true : false;
		n = (n ^ (n >> 31)) - (n >> 31);
		while (n > 0) {
			if ((n & 1) == 1)
				res *= x;
			x *= x;
			n >>= 1;
		}
		return is_neg ? 1.0 / res : res;
	}
}
