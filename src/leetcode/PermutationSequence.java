package leetcode;

/*The set [1,2,3,…,n] contains a total of n! unique permutations.
 By listing and labeling all of the permutations in order,
 We get the following sequence (ie, for n = 3):
 "123"
 "132"
 "213"
 "231"
 "312"
 "321"
 Given n and k, return the kth permutation sequence.
 Note: Given n will be between 1 and 9 inclusive.
 Idea: 
 * The k / (n-1)! tells us which number should be placed. Note k-- beforehand.
 * Use LinkedList to hold digits to facilitate removing used number.*/
import java.util.LinkedList;

public class PermutationSequence {

	public String getPermutation(int n, int k) {
		StringBuilder sb = new StringBuilder();
		LinkedList<Integer> digits = new LinkedList<Integer>();
		for (int i = 1; i <= n; i++)
			digits.add(i);
		k--;
		while (n > 0) {
			int pos = k / factorial(n - 1);
			int digit = digits.remove(pos);
			sb.append(digit);
			k = k % factorial(n - 1);
			n--;
		}
		return sb.toString();
	}

	public int factorial(int n) {
		if (n <= 1)
			return 1;
		int ret = 1;
		while (n > 1) {
			ret *= n;
			n--;
		}
		return ret;
	}
}
