package leetcode;

/**
 * Search a 2D Matrix Write an efficient algorithm that searches for a value in
 * an m x n matrix. This matrix has the following properties: Integers in each
 * row are sorted from left to right. The first integer of each row is greater
 * than the last integer of the previous row. For example,Consider the following
 * matrix: [ [1, 3, 5, 7], [10, 11, 16, 20], [23, 30, 34, 50] ] Given target =
 * 3, return true.
 * 
 * Idea: Treat matrix as one dimensional array and apply binary search.
 * */
public class Search2DMatrix {

	public boolean searchMatrix(int[][] matrix, int target) {
		if (matrix.length == 0)
			return false;
		int total = matrix.length * matrix[0].length, perRow = matrix[0].length;
		int begin = 0, end = total - 1;
		while (begin <= end) {
			int mid = begin + (end - begin) / 2;
			int row = mid / perRow;
			int col = mid % perRow;
			if (matrix[row][col] == target)
				return true;
			if (matrix[row][col] < target)
				begin = mid + 1;
			else
				end = mid - 1;
		}
		return false;
	}
}