package leetcode;

/*Single Number Given an array of integers, every element appears twice except
 * for one. Find that single one. Note: Your algorithm should have a linear
 * runtime complexity. Could you implement it without using extra memory?
 * Idea: Key: x ^ x = 0 and 0 ^ x = x.
 * */

public class SingleNumber {

	public int singleNumber(int[] A) {
		int ret = 0;
		for (int i = 0; i < A.length; i++)
			ret = ret ^ A[i];
		return ret;
	}
}