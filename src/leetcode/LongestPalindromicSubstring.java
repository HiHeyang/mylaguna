package leetcode;

/*Given a string S, find the longest palindromic substring in S. 
 * You may assume that the maximum length of S is 1000, 
 * and there exists one unique longest palindromic substring.
 * e.g. "abac" returns "aba"
 * Find longest palindrome for each str[i] and (str[i],str[i+1]) as the middle string.
 * Also, there is DP solution.
 * */
public class LongestPalindromicSubstring {

	public String longestPalindrome(String s) {
		String ret = "";
		for (int i = 0; i < s.length(); i++) {
			String palindrome = getPalindrome(s, i, i);
			if (palindrome.length() > ret.length())
				ret = palindrome;
			palindrome = getPalindrome(s, i, i + 1);
			if (palindrome.length() > ret.length())
				ret = palindrome;
		}
		return ret;
	}

	private String getPalindrome(String str, int left, int right) {
		while (left >= 0 && right < str.length()
				&& str.charAt(left) == str.charAt(right)) {
			left--;
			right++;
		}
		return str.substring(left + 1, right);
	}
}
