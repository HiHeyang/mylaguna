package leetcode;

/*Given a linked list and a value x, partition it such that all nodes less than x 
 come before nodes greater than or equal to x.
 You should preserve the original relative order of the nodes in each of 
 the two partitions.
 For example,
 Given 1->4->3->2->5->2 and x = 3,
 return 1->2->2->4->3->5.
 Idea: Append newly-created bigger node to end of list. Three pointers. newHead 
 with 0 as value. curr and end. Use cnt to avoid infinite loop.*/
public class PartitionList {

	public ListNode partition(ListNode head, int x) {
		if (head == null)
			return null;
		ListNode newHead = new ListNode(0), curr = newHead, end = head;
		newHead.next = head;
		int cnt = 1;
		while (end.next != null) {
			cnt++;
			end = end.next;
		}
		while (cnt > 0) {
			if (curr.next.val >= x) {
				end.next = new ListNode(curr.next.val);
				curr.next = curr.next.next;
				end = end.next;
			} else
				curr = curr.next;
			cnt--;
		}
		return newHead.next;
	}
}
