package leetcode;

/*Given a matrix of m x n elements (m rows, n columns), 
 * return all elements of the matrix in spiral order.
 For example,
 Given the following matrix:
 [
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
 ]
 You should return [1,2,3,6,9,8,7,4,5].*/
import java.util.ArrayList;

public class SpiralMatrix {

	public ArrayList<Integer> spiralOrder(int[][] matrix) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
			return res;
		spiralOrder(matrix, 0, 0, matrix.length, matrix[0].length, res);
		return res;
	}

	public void spiralOrder(int[][] matrix, int x, int y, int m, int n,
			ArrayList<Integer> res) {
		if (m == 0 || n == 0)
			return;
		if (m == 1 && n == 1) {
			res.add(matrix[x][y]);
			return;
		}
		for (int i = 0; i < n - 1; i++)
			res.add(matrix[x][y++]);
		for (int i = 0; i < m - 1; i++)
			res.add(matrix[x++][y]);
		if (m > 1) {
			for (int i = 0; i < n - 1; i++)
				res.add(matrix[x][y--]);
		}
		if (n > 1) {
			for (int i = 0; i < m - 1; i++)
				res.add(matrix[x--][y]);
		}
		if (m == 1 || n == 1)
			spiralOrder(matrix, x, y, 1, 1, res);
		else
			spiralOrder(matrix, x + 1, y + 1, m - 2, n - 2, res);
	}
}
