package leetcode;

/*Given a linked list, swap every two adjacent nodes and return its head.
 For example,
 Given 1->2->3->4, you should return the list as 2->1->4->3.
 Your algorithm should use only constant space. 
 You may not modify the values in the list, only nodes itself can be changed.*/
public class SwapNodesinPairs {

	public ListNode swapPairs(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode pre = new ListNode(0);
		ListNode result = pre;
		pre.next = head;
		ListNode now = head;
		ListNode next;
		while (now != null && now.next != null) {
			next = now.next;
			pre.next = next;
			now.next = next.next;
			next.next = now;
			pre = now;
			now = now.next;
		}
		return result.next;
	}
}
