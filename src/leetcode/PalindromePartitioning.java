package leetcode;

/*Given a string s, partition s such that every substring of the partition is a palindrome.
 Return all possible palindrome partitioning of s.
 For example, given s = "aab",
 Return
 [
 ["aa","b"],
 ["a","a","b"]
 ]*/
import java.util.ArrayList;
import java.util.List;

public class PalindromePartitioning {

	public List<List<String>> partition(String s) {
		List<List<String>> result = new ArrayList<List<String>>();
		if (s == null || s.length() == 0)
			return result;
		int index = 0;
		List<String> partition = new ArrayList<String>();
		dfs(s, index, partition, result);
		return result;
	}

	private void dfs(String s, int index, List<String> partition,
			List<List<String>> result) {
		if (index == s.length()) {
			List<String> temp = new ArrayList<String>(partition);
			result.add(temp);
			return;
		}
		for (int i = index + 1; i <= s.length(); i++) {
			String tempStr = s.substring(index, i);
			if (isPalindrome(tempStr)) {
				partition.add(tempStr);
				dfs(s, i, partition, result);
				partition.remove(partition.size() - 1);
			}
		}
	}

	private boolean isPalindrome(String str) {
		int left = 0;
		int right = str.length() - 1;
		while (left < right) {
			if (str.charAt(left) != str.charAt(right))
				return false;
			left++;
			right--;
		}
		return true;
	}
}
