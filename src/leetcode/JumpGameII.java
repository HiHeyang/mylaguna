package leetcode;

/*Given an array of non-negative integers, you are initially positioned 
 *at the first index of the array.
 Each element in the array represents your maximum jump length at that position.
 Your goal is to reach the last index in the minimum number of jumps.
 For example:
 Given array A = [2,3,1,1,4]
 The minimum number of jumps to reach the last index is 2. 
 (Jump 1 step from index 0 to 1, then 3 steps to the last index.)
 Use HashSet instead of ArrayList can pass the large set of data.
 idea: greedy algorithm.pick the position that could reach the farthest position.*/
public class JumpGameII {

	public int jump(int[] A) {
		if (A == null || A.length <= 1)
			return 0;
		int len = A.length;
		int max = 0, steps = 0;
		int i = 0;
		while (i < len) {
			max = Math.max(max, A[i] + i);
			if (max >= len - 1)
				return ++steps;
			steps++;
			int tmpMax = 0;
			for (int j = i + 1; j <= max; j++) {
				if (A[j] + j > tmpMax) {
					tmpMax = A[j] + j;
					i = j;
				}
			}
		}
		return steps;
	}
}
