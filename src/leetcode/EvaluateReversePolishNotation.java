package leetcode;

import java.util.Stack;

public class EvaluateReversePolishNotation {

	public int evalRPN(String[] tokens) {
		Stack<String> stack = new Stack<String>();
		for (String s : tokens) {
			if (s.equals("+") || s.equals("-") || s.equals("*")
					|| s.equals("/")) {
				String a = stack.pop();
				String b = stack.pop();
				int res = arithmetic(Integer.parseInt(a), Integer.parseInt(b),
						s);
				stack.push(String.valueOf(res));
			} else
				stack.push(s);
		}
		return stack.isEmpty() ? 0 : Integer.parseInt(stack.pop());
	}

	public int arithmetic(int a, int b, String op) {
		if (op.equals("+"))
			return b + a;
		else if (op.equals("-"))
			return b - a;
		else if (op.equals("*"))
			return b * a;
		else
			return b / a;
	}
}
