package leetcode;

/*
 * Pascal Triangle II
 * Given an index k, return the kth row of the Pascal's triangle. 
 * For example, given k = 3, Return [1,3,3,1]. 
 * Note:Could you optimize your algorithm to use only O(k) extra space?
 * 
 * Idea: k[rowIndex+1]. Start with setting the end of row.
 * */
import java.util.ArrayList;

public class PascalsTriangleII {

	public ArrayList<Integer> getRow(int rowIndex) {
		ArrayList<Integer> row = new ArrayList<Integer>();
		if (rowIndex < 0)
			return row;
		int[] k = new int[rowIndex + 1];
		for (int i = 0; i <= rowIndex; i++) {
			for (int j = i; j >= 0; j--) {
				if (j == 0 || j == i)
					k[j] = 1;
				else
					k[j] = k[j - 1] + k[j];

			}
		}
		for (int num : k)
			row.add(num);
		return row;
	}
}
