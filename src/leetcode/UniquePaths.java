package leetcode;

/*A robot is located at the top-left corner of a m x n grid (marked 'Start' in the diagram below).
 The robot can only move either down or right at any point in time. 
 The robot is trying to reach the bottom-right corner of the grid (marked 'Finish' in the diagram below).
 How many possible unique paths are there?
 Above is a 3 x 7 grid. How many possible unique paths are there?
 Note: m and n will be at most 100.*/

public class UniquePaths {

	public int uniquePaths_one(int m, int n) {
		if (--m > --n) {
			int temp = m;
			m = n;
			n = temp;
		}
		long denominator = 1, numerator = 1;
		for (int i = 1; i <= m; i++)
			denominator *= i;
		for (int i = m + n; i > n; i--)
			numerator *= i;
		return (int) (numerator / denominator);
	}

	// DP
	public int uniquePaths_two(int m, int n) {
		int[][] table = new int[m][n];
		table[0][0] = 1;
		for (int i = 1; i < m; i++)
			table[i][0] = 1;
		for (int j = 1; j < n; j++)
			table[0][j] = 1;
		for (int i = 1; i < m; i++) {
			for (int j = 1; j < n; j++)
				table[i][j] = table[i][j - 1] + table[i - 1][j];
		}
		return table[m - 1][n - 1];
	}
}
