package leetcode;

/*Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
 For example, given n = 3, a solution set is:
 "((()))", "(()())", "(())()", "()(())", "()()()"*/
import java.util.ArrayList;

public class GenerateParentheses {

	public ArrayList<String> generateParenthesis(int n) {
		ArrayList<String> res = new ArrayList<String>();
		genParen(n, n, "", res);
		return res;
	}

	public void genParen(int l, int r, String str, ArrayList<String> res) {
		if (l == 0 && r == 0) {
			res.add(str);
			return;
		}
		if (l > 0)
			genParen(l - 1, r, str + "(", res);
		if (r > l)
			genParen(l, r - 1, str + ")", res);
	}
}
