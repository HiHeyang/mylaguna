package leetcode;

/*Given a collection of numbers, return all possible permutations.
 For example,
 [1,2,3] have the following permutations:
 [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], and [3,2,1].*/
import java.util.ArrayList;
import java.util.List;

public class Permutations {

	public List<List<Integer>> permute(int[] num) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		perm(num, 0, ret);
		return ret;
	}

	public void perm(int[] num, int index, List<List<Integer>> ret) {
		if (index == num.length) {
			ArrayList<Integer> item = new ArrayList<Integer>();
			for (int k : num)
				item.add(k);
			ret.add(item);
			return;
		}
		for (int i = index; i < num.length; i++) {
			swap(num, index, i);
			perm(num, index + 1, ret);
			swap(num, index, i);
		}
	}

	public void swap(int[] num, int j, int k) {
		int tmp = num[k];
		num[k] = num[j];
		num[j] = tmp;
	}
}
