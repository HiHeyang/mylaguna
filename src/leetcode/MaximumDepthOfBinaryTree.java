package leetcode;

/*Given a binary tree, find its maximum depth.
 The maximum depth is the number of nodes along the longest path from the root node down to 
 the farthest leaf node.*/
import java.util.ArrayList;

public class MaximumDepthOfBinaryTree {

	// version1
	public int maxDepthOne(TreeNode root) {
		if (root == null)
			return 0;
		return 1 + Math.max(maxDepthOne(root.left), maxDepthOne(root.right));
	}

	// version2
	public int maxDepthTwo(TreeNode root) {
		ArrayList<TreeNode> prev = new ArrayList<TreeNode>();
		if (root != null)
			prev.add(root);
		int depth = 0;
		while (!prev.isEmpty()) {
			ArrayList<TreeNode> temp = new ArrayList<TreeNode>();
			for (TreeNode node : prev) {
				if (node.left != null)
					temp.add(node.left);
				if (node.right != null)
					temp.add(node.right);
			}
			prev = new ArrayList<TreeNode>(temp);
			depth++;
		}
		return depth;
	}
}
