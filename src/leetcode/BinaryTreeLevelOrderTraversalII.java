package leetcode;
/*
 * Input tree:
 *     1
 *    / \
 *   2   3
 *  / \   \
 * 4   5   6  
 *          \
 *           7
 * Level by level bottom-up: 7 4 5 6 2 3 1
 * */
import java.util.ArrayList;
import java.util.LinkedList;

public class BinaryTreeLevelOrderTraversalII {

	public ArrayList<ArrayList<Integer>> levelOrderBottom(TreeNode root) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
		if (root == null)
			return list;
		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		queue.addLast(root);
		int currentCnt = 1, nextCnt = 0;		
		ArrayList<Integer> level = new ArrayList<Integer>();
		while (!queue.isEmpty()) {
			TreeNode node = queue.removeFirst();
			level.add(node.val);
			currentCnt--;
			if (node.left != null) {
				queue.addLast(node.left);
				nextCnt++;
			}
			if (node.right != null) {
				queue.addLast(node.right);
				nextCnt++;
			}
			if (currentCnt == 0) {
				list.add(level);
				level = new ArrayList<Integer>();
				currentCnt = nextCnt;
				nextCnt = 0;
			}
		}
		ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>(
				list.size());
		for (int i = list.size() - 1; i >= 0; i--)
			ret.add(list.get(i));
		return ret;
	}
}
