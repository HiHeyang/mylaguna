package leetcode;

/*You are given two linked lists representing two non-negative numbers. 
 * The digits are stored in reverse order and each of their nodes contain a single digit. 
 * Add the two numbers and return it as a linked list.
 Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 Output: 7 -> 0 -> 8*/

public class AddTwoNumbers {
	
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		return addLists(l1, l2, 0);
	}

	public ListNode addLists(ListNode l1, ListNode l2, int carry) {
		if (l1 == null && l2 == null) {
			if (carry == 1)
				return new ListNode(1);
			return null;
		}
		ListNode result = new ListNode(0);
		int value = carry;
		if (l1 != null)
			value += l1.val;
		if (l2 != null)
			value += l2.val;
		result.val = value % 10;
		ListNode before = addLists(l1 == null ? null : l1.next,
				l2 == null ? null : l2.next, value >= 10 ? 1 : 0);
		result.next = before;
		return result;
	}
}
