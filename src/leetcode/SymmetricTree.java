package leetcode;

/*Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).
 For example, this binary tree is symmetric:
 1
 / \
 2   2
 / \ / \
 3  4 4  3
 But the following is not:
 1
 / \
 2   2
 \   \
 3    3
 Note: Bonus points if you could solve it both recursively and iteratively.*/
import java.util.LinkedList;

public class SymmetricTree {

	// Recursion
	public boolean isSymmetric_Recursion(TreeNode root) {
		if (root == null)
			return true;
		return isSymmetric(root.left, root.right);
	}

	public boolean isSymmetric(TreeNode p, TreeNode q) {
		if (p == null && q == null)
			return true;
		if (p == null && q != null || p != null && q == null)
			return false;
		if (p != null && q != null) {
			if (p.val != q.val)
				return false;
			else
				return isSymmetric(p.left, q.right)
						&& isSymmetric(p.right, q.left);
		}
		return false;
	}

	// Iteration
	public boolean isSymmetric_Iteration(TreeNode root) {
		if (root == null)
			return true;
		LinkedList<TreeNode> l = new LinkedList<TreeNode>(), r = new LinkedList<TreeNode>();
		l.add(root.left);
		r.add(root.right);
		while (!l.isEmpty() && !r.isEmpty()) {
			TreeNode temp1 = l.poll(), temp2 = r.poll();
			if (temp1 == null && temp2 != null || temp1 != null
					&& temp2 == null)
				return false;
			if (temp1 != null) {
				if (temp1.val != temp2.val)
					return false;
				l.add(temp1.left);
				l.add(temp1.right);
				r.add(temp2.right);
				r.add(temp2.left);
			}
		}
		return true;
	}
}
