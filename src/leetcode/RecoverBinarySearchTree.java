package leetcode;

/*Two elements of a binary search tree (BST) are swapped by mistake.
 Recover the tree without changing its structure.
 Note: A solution using O(n) space is pretty straight forward. Could you devise a constant space solution?*/
import java.util.ArrayList;

public class RecoverBinarySearchTree {

	private ArrayList<TreeNode> list = new ArrayList<TreeNode>();
	private TreeNode prevNode = null;

	public void recoverTree(TreeNode root) {
		if (root == null)
			return;
		inorderWithPrevNode(root);
		swap();
	}

	private void inorderWithPrevNode(TreeNode root) {
		if (root.left != null)
			inorderWithPrevNode(root.left);
		if (prevNode != null)
			if (prevNode.val > root.val) {
				list.add(prevNode);
				list.add(root);
			}
		prevNode = root;
		if (root.right != null)
			inorderWithPrevNode(root.right);
	}

	private void swap() {
		TreeNode begin = list.get(0);
		TreeNode end = list.get(list.size() - 1);
		int tmp = end.val;
		end.val = begin.val;
		begin.val = tmp;
	}
}
