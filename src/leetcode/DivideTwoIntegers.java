package leetcode;

/*Divide two integers without using multiplication, division and mod operator.
 *trick: ((dividend ^ divisor) >>> 31) == 1 check two number are of same sign.*/
public class DivideTwoIntegers {

	public int divide(int dividend, int divisor) {
		long a = Math.abs((long) dividend);
		long b = Math.abs((long) divisor);
		int k = 0;
		while (b << k < a)
			k++;
		int result = 0;
		for (int i = k; i >= 0; i--) {
			long temp = b << i;
			if (a >= temp) {
				result |= 1 << i;
				a -= temp;
			}
		}
		return (((dividend ^ divisor) >>> 31) == 1) ? -result : result;
	}
}
