package leetcode;

class Node {
    public int value;
    public Node left, right;

    public Node(int value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}

public class Test {
    public static boolean isValidBST(Node root) {
    	return isBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
        //throw new UnsupportedOperationException("Waiting to be implemented.");
    }
    private static boolean isBST(Node root, int min, int max) {
		if (root == null)
			return true;
		if (root.value <= min || root.value >= max)
			return false;
		return (isBST(root.left, min, root.value) && isBST(root.right, root.value,
				max));
	}

    public static void main(String[] args) {
        Node n1 = new Node(1, null, null);
        Node n3 = new Node(3, null, null);
        Node n2 = new Node(2, n1, n3);
        String a = ",,,";

        System.out.println(a.split(",",-2).length);
    }
}
