package leetcode;

import java.util.Stack;

/*Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
 For "(()", the longest valid parentheses substring is "()", which has length = 2.
 Another example is ")()())", where the longest valid parentheses substring is "()()", which has length = 4.*/
public class LongestValidParentheses {

	public int longestValidParentheses(String s) {
		char[] c = s.toCharArray();
		Stack<Integer> store = new Stack<Integer>();
		int[] corresponding = new int[c.length];
		int res = 0;
		for (int i = 0; i < c.length; i++) {
			if (c[i] == '(')
				store.push(i);
			else {
				if (!store.isEmpty()) {
					corresponding[i] = i - store.peek() + 1;
					int temp = store.pop() - 1;
					if (temp >= 0 && corresponding[temp] > 0)
						corresponding[i] += corresponding[temp];
					res = Math.max(corresponding[i], res);
				}
			}
		}
		return res;
	}
}
