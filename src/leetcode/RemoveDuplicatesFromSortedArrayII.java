package leetcode;

/*Follow up for "Remove Duplicates": What if duplicates are allowed at most twice?
 For example, Given sorted array A = [1,1,1,2,2,3],
 Your function should return length = 5, and A is now [1,1,2,2,3].
 fast and slow pointers. Slow is the end index of array containing duplicates of two
 at most. fast starts with index 1. dupCnt start with 1.*/
public class RemoveDuplicatesFromSortedArrayII {

	public int removeDuplicates(int[] A) {
		int len = A.length, fast = 1, slow = 0, dupCnt = 1;
		if (len == 0)
			return 0;
		while (fast < len) {
			if (A[fast] != A[slow]) {
				A[++slow] = A[fast];
				dupCnt = 1;
			} else {
				if (dupCnt < 2) {
					dupCnt++;
					A[++slow] = A[fast];
				}
			}
			fast++;
		}
		return ++slow;
	}
}
