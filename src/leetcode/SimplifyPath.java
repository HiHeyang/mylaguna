package leetcode;

/*
 * Simplify Path
 * Given an absolute path for a file (Unix-style), simplify it.
 * For example,
 * path = "/home/", => "/home"
 * path = "/a/./b/../../c/", => "/c"
 * Corner Cases:
 * Did you consider the case where path = "/../"?
 * In this case, you should return "/".
 * Another corner case is the path might contain multiple slashes '/'
 * together, such as "/home//foo/". In this case, you should ignore
 * redundant slashes and return "/home/foo".
 * 
 * Idea:
 * Split path into parts by delimiter "/+". Use stack to store the path.
 * When meeting "..", pop stack.
 * */
import java.util.Stack;

public class SimplifyPath {

	public String simplifyPath(String path) {
		String[] parts = path.split("/+");
		Stack<String> stack = new Stack<String>();
		for (String part : parts) {
			if (part.equals(""))
				continue;
			if (!part.equals(".") && !part.equals(".."))
				stack.push(part);
			else {
				if (part.equals("..") && !stack.isEmpty())
					stack.pop();
			}
		}
		if (stack.isEmpty())
			return "/";
		StringBuilder sb = new StringBuilder();
		for (String p : stack) {
			sb.append("/");
			sb.append(p);
		}
		return sb.toString();
	}
}
