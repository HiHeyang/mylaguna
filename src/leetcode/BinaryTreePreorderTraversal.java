package leetcode;

import java.util.ArrayList;
import java.util.Stack;

public class BinaryTreePreorderTraversal {

	// recursion
	public ArrayList<Integer> preorderTraversal_Recursion(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		pre(res, root);
		return res;
	}

	public void pre(ArrayList<Integer> res, TreeNode root) {
		res.add(root.val);
		if (root.left != null)
			pre(res, root.left);
		if (root.right != null)
			pre(res, root.right);
	}

	// iteration
	public ArrayList<Integer> preorderTraversal_Iteration(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		Stack<TreeNode> s = new Stack<TreeNode>();
		TreeNode cur = root;
		while (!s.isEmpty() || cur != null) {
			if (cur != null) {
				res.add(cur.val);
				s.push(cur);
				cur = cur.left;
			} else {
				cur = s.pop();
				cur = cur.right;
			}
		}
		return res;
	}
}
