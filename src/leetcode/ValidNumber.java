package leetcode;

/*Validate if a given string is numeric.
 Some examples:
 "0" => true
 " 0.1 " => true
 "abc" => false
 "1 a" => false
 "2e10" => true
 Note: It is intended for the problem statement to be ambiguous. 
 You should gather all requirements up front before implementing one.
 trim two sides check if there is e
 if has e check if there left are number and right side is integer.*/
public class ValidNumber {

	public boolean isNumber(String s) {
		if (s == null || s.length() == 0)
			return false;
		s = s.trim();
		int i = 0, len = s.length()-1;
		while(i <= len) {
			if (s.charAt(i) == 'e' || s.charAt(i) == 'E') {
				return isRegularNumber(s, 0, i - 1)
						&& isInteger(s, i + 1, len);
			}
			i++;
		}
		return isRegularNumber(s, 0, len);
	}

	public boolean isRegularNumber(String s, int start, int end) {
		if (start <= end) {
			boolean hasDot = false;
			boolean hasDigit = false;
			if (s.charAt(start) == '+' || s.charAt(start) == '-')
				start++;
			for (int i = start; i <= end; ++i) {
				if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
					hasDigit = true;
					continue;
				}
				if (s.charAt(i) == '.' && !hasDot)
					hasDot = true;
				else
					return false;
			}
			return hasDigit;
		}
		return false;
	}

	public boolean isInteger(String s, int start, int end) {
		if (start <= end) {
			boolean hasDigit = false;
			if (s.charAt(start) == '+' || s.charAt(start) == '-')
				start++;
			for (int i = start; i <= end; i++) {
				if (s.charAt(i) >= '0' && s.charAt(i) <= '9') {
					hasDigit = true;
					continue;
				} else
					return false;
			}
			return hasDigit;
		}
		return false;
	}
}
