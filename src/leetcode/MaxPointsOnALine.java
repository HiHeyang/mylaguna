package leetcode;
//Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.
import java.util.HashMap;

public class MaxPointsOnALine {

	public int maxPoints(Point[] points) {
		if (points == null || points.length == 0)
			return 0;
		HashMap<Double, Integer> map = new HashMap<Double, Integer>();
		int max = 1;
		for (int i = 0; i < points.length; i++) {
			map.clear();
			map.put((double) Integer.MIN_VALUE, 1);
			int dup = 0;
			for (int j = i + 1; j < points.length; j++) {
				if (points[j].x == points[i].x && points[j].y == points[i].y) {
					dup++;
					continue;
				}
				double slope = points[j].x - points[i].x == 0 ? Integer.MAX_VALUE
						: 0.0 + (double) (points[j].y - points[i].y)
								/ (double) (points[j].x - points[i].x);
				if (map.containsKey(slope))
					map.put(slope, map.get(slope) + 1);
				else
					map.put(slope, 2);
			}
			for (int temp : map.values()) 
				max = Math.max(max, dup+temp);
		}
		return max;
	}
}
