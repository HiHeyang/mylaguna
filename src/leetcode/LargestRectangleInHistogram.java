package leetcode;

/*Given n non-negative integers representing the histogram's bar height where 
 *the width of each bar is 1, find the area of largest rectangle in the histogram.
 Above is a histogram where width of each bar is 1, given height = [2,1,5,6,2,3].
 The largest rectangle is shown in the shaded area, which has area = 10 unit.
 For example, Given height = [2,1,5,6,2,3], return 10.*/
/* Data structure: Stack. Push bigger bar to stack. Otherwise, calculate area
 * with stack.pop() as smallest bar. Left smaller is at stack.peek() and right
 * smaller is at i. Another while loop check remaining bars.
 * 
 */
import java.util.Stack;

public class LargestRectangleInHistogram {

	public int largestRectangleArea(int[] height) {
		int max = 0, i = 0, size = height.length;
		Stack<Integer> stack = new Stack<Integer>();
		while (i < size) {
			if (stack.empty() || height[i] >= height[stack.peek()])
				stack.push(i++);
			else {
				int topIndex = stack.pop();
				int area = height[topIndex]
						* (stack.empty() ? i : (i - stack.peek() - 1));
				max = Math.max(max, area);
			}
		}
		while (!stack.isEmpty()) {
			int indexTop = stack.pop();
			int area = height[indexTop]
					* (stack.empty() ? i : (i - stack.peek() - 1));
			max = Math.max(max, area);
		}
		return max;
	}
}
