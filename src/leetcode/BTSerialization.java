package leetcode;

public class BTSerialization {
	
	private StringBuffer sb = new StringBuffer("");

	public String btserialization(TreeNode root) {
		if (root == null)
			sb.append("# ");
		else {
			sb.append(root.val + " ");
			btserialization(root.left);
			btserialization(root.right);
		}
		return sb.toString();
	}

	public static void main(String[] args) {
		TreeNode a = new TreeNode(30);
		TreeNode b = new TreeNode(10);
		TreeNode c = new TreeNode(50);
		TreeNode d = new TreeNode(20);
		TreeNode e = new TreeNode(45);
		TreeNode f = new TreeNode(35);
		a.left = b;
		a.right = d;
		b.left = c;
		d.left = e;
		d.right = f;
		BTSerialization bts = new BTSerialization();
		System.out.println(bts.btserialization(a));
	}
}
