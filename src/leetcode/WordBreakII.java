package leetcode;

/*Given a string s and a dictionary of words dict, add spaces in s to construct a sentence 
  where each word is a valid dictionary word.
 Return all such possible sentences.
 For example, given
 s = "catsanddog",
 dict = ["cat", "cats", "and", "sand", "dog"].
 A solution is ["cats and dog", "cat sand dog"].*/
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordBreakII {

	public List<String> wordBreak(String s, Set<String> dict) {
		int len = s.length();
		boolean f[] = new boolean[len + 1];
		ArrayList<ArrayList<Integer>> prev = new ArrayList<ArrayList<Integer>>();
		for (int i = 0; i <= len; i++)
			prev.add(new ArrayList<Integer>());
		f[0] = true;
		for (int i = 1; i <= len; i++) {
			for (int j = i - 1; j >= 0; j--) {
				if (f[j] && dict.contains(s.substring(j, i))) {
					prev.get(i).add(j);
					f[i] = true;
				}
			}
		}
		List<String> result = new ArrayList<String>();
		dfs(s, prev, len, result, "");
		return result;
	}

	private void dfs(String s, ArrayList<ArrayList<Integer>> prev,
			int endindex, List<String> result, String current) {
		ArrayList<Integer> prevs = prev.get(endindex);
		for (int i = 0; i < prevs.size(); i++) {
			int index = prevs.get(i);
			String sub = s.substring(index, endindex);
			String r = sub;
			if (!current.equals(""))
				r = r + " " + current;
			if (index == 0)
				result.add(r);
			else
				dfs(s, prev, index, result, r);
		}
	}
	
	
	
	public List<String> wordBreak2(String s, Set<String> dict) {
		List<String> result = new ArrayList<String>();
		if (s == null || s.length() == 0)
			return result;
		ArrayList<String> partition = new ArrayList<String>();
		dfs(s, 0, partition, result, dict);
		return result;
	}

	private void dfs(String s, int index, ArrayList<String> partition,
			List<String> result, Set<String> dict) {
		if (index == s.length()) {
			StringBuilder sb = new StringBuilder();
			for(int j = 0;j < partition.size();j++){
				if(j == 0)sb.append(partition.get(j));
				else sb.append(" "+partition.get(j));
			}
			result.add(sb.toString());
			return;
		}
		for (int i = index + 1; i <= s.length(); i++) {
			String tempStr = s.substring(index, i);
			if (dict.contains(tempStr)) {
				partition.add(tempStr);
				dfs(s, i, partition, result, dict);
				partition.remove(partition.size()-1);
			}
		}
	}
	
	public static void main(String[] args){
		WordBreakII wb = new WordBreakII();
		String s = "catsanddog";
		Set<String> dict = new HashSet<String>();
		dict.add("cat");
		dict.add("cats");
		dict.add("and");
		dict.add("sand");
		dict.add("dog");
		dict.add("c");
		dict.add("a");
		List<String> result = wb.wordBreak2(s, dict);
		for(String st:result)System.out.println(st);
	}
}
