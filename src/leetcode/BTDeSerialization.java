package leetcode;

import java.util.LinkedList;

public class BTDeSerialization {

	public TreeNode btDeSerialization(LinkedList<String> list) {

		if (list.isEmpty())
			return null;
		String str = list.removeFirst();
		if (str.equals("#"))
			return null;
		TreeNode p = new TreeNode(Integer.parseInt(str));
		p.left = btDeSerialization(list);
		p.right = btDeSerialization(list);
		return p;
	}

	public void pre(TreeNode t) {
		if (t != null) {
			System.out.print(t.val + " ");
			pre(t.left);
			pre(t.right);
		}
	}

	public static void main(String[] args) {
		String[] s = { "30", "10", "50", "#", "#", "#", "20", "45", "#", "#",
				"35", "#", "#" };
		LinkedList<String> list = new LinkedList<String>();
		for (String str : s)
			list.add(str);
		BTDeSerialization btds = new BTDeSerialization();
		btds.pre(btds.btDeSerialization(list));
	}
}
