package leetcode;

public class ReverseLinkedList {

	// Recursive
	public ListNode reverseListRecursive(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode nextNode = head.next;
		ListNode newHead = reverseListRecursive(nextNode);
		head.next = null;
		nextNode.next = head;
		return newHead;
	}

	// iterative
	public ListNode reverseListIterative(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode prev = null, current = head;
		while (current != null) {
			ListNode next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		return prev;
	}
}
