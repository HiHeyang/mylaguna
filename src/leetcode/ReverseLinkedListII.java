package leetcode;

/*Reverse a linked list from position m to n. Do it in-place and in one-pass.
 For example:
 Given 1->2->3->4->5->NULL, m = 2 and n = 4,
 return 1->4->3->2->5->NULL.
 Note:
 Given m, n satisfy the following condition:
 1 ≤ m ≤ n ≤ length of list.*/
public class ReverseLinkedListII {

	public ListNode reverseBetween(ListNode head, int m, int n) {
		if (head == null || head.next == null)
			return head;
		ListNode prev = new ListNode(0);
		prev.next = head;
		head = prev;
		ListNode cur = head.next;
		int k = m - 1;
		while (k > 0) {
			prev = cur;
			cur = cur.next;
			k--;
		}
		k = n - m;
		while (cur.next != null && k > 0) {
			ListNode next = cur.next;
			cur.next = next.next;
			next.next = prev.next;
			prev.next = next;
			k--;
		}
		return head.next;
	}
}
