package leetcode;
/*Write a function to find the longest common prefix string amongst an array of strings.*/
public class LongestCommonPrefix {

	public String longestCommonPrefix(String[] strs) {
		if (strs == null || strs.length == 0)
			return "";
		if (strs.length == 1)
			return strs[0];
		String str = strs[0];
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);
			for (int j = 1; j < strs.length; j++) {
				if (strs[j].length() == i || strs[j].charAt(i) != c)
					return str.substring(0, i);
			}
		}
		return str;
	}
}
