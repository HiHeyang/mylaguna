package leetcode;

/*Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
 For example, S = "ADOBECODEBANC" T = "ABC"
 Minimum window is "BANC".
 Note: If there is no such window in S that covers all characters in T, return the empty string "".
 If there are multiple such windows, you are guaranteed that there will always be only one unique minimum window in S.
 Data structure: two int[256] toFind and found. Keep start and end of current min Substring. 
 Skip non-toFind char. If count == tLen, 1) advance begin index and update minBegin and minEnd and minLen.*/
public class MinimumWindowSubstring {

	public String minWindow(String S, String T) {
		int[] toFind = new int[256];
		int tLen = T.length();
		for (int i = 0; i < tLen; i++)
			toFind[T.charAt(i)]++;
		int[] found = new int[256];
		int minLen = Integer.MAX_VALUE, minBegin = 0, minEnd = 0, count = 0;
		for (int begin = 0, end = 0; end < S.length(); end++) {
			int cha = S.charAt(end);
			if (toFind[cha] == 0)
				continue;
			found[cha]++;
			if (found[cha] <= toFind[cha])
				count++;
			if (count == tLen) {
				int beginChar = S.charAt(begin);
				while (toFind[beginChar] == 0
						|| found[beginChar] > toFind[beginChar]) {
					if (found[beginChar] > toFind[beginChar])
						found[beginChar]--;
					beginChar = S.charAt(++begin);
				}
				int currLen = end - begin + 1;
				if (currLen < minLen) {
					minBegin = begin;
					minEnd = end;
					minLen = currLen;
				}
			}
		}
		return (count == tLen) ? S.substring(minBegin, minEnd + 1) : "";
	}
}
