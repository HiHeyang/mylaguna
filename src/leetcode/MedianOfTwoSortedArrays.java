package leetcode;

/*There are two sorted arrays A and B of size m and n respectively.
 *Find the median of the two sorted arrays. 
 *The overall run time complexity should be O(log (m+n)).
 *ref: http://yucoding.blogspot.com/2013/01/leetcode-question-50-median-of-two.html
 */
import java.util.Arrays;

public class MedianOfTwoSortedArrays {
	public double findMedianSortedArrays(int A[], int B[]) {
		int m = A.length, n = B.length, total = m + n;
		if (total % 2 == 1)
			return findKth(A, B, total / 2 + 1);
		else
			return (findKth(A, B, total / 2) + findKth(A, B, total / 2 + 1)) / 2.0;
	}

	double findKth(int[] A, int[] B, int k) {
		int m = A.length, n = B.length;
		if (m > n)
			return findKth(B, A, k);
		if (m == 0)
			return B[k - 1];
		if (k == 1)
			return Math.min(A[0], B[0]);
		int dropA = Math.min(k / 2, m);
		int dropB = k - dropA;
		if (A[dropA - 1] <= B[dropB - 1]) {
			int[] newA = Arrays.copyOfRange(A, dropA, m);
			return findKth(newA, B, k - dropA);
		}
		int[] newB = Arrays.copyOfRange(B, dropB, n);
		return findKth(A, newB, k - dropB);
	}
}