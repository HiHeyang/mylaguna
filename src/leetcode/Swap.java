package leetcode;

public class Swap {

	public static void swap(int a, int b) {
		a = b - a; // difference(second-first)
		b = b - a; // second-diff=first
		a = a + b; // diff+first=second
	}
}
