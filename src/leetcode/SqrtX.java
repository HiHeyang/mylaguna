package leetcode;

/*Implement int sqrt(int x).
 Compute and return the square root of x.*/
public class SqrtX {

	public int sqrtx(int x) {
		int low = 0;
		int high = x;
		if (x < 2)
			return x;
		while (low <= high) {
			int mid = low + (high - low) / 2;
			if (x / mid == mid)
				return mid;
			if (x / mid < mid)
				high = mid - 1;
			else
				low = mid + 1;
		}
		return high;
	}
}
