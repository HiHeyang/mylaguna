package leetcode;

/*Given a string s consists of upper/lower-case alphabets and empty space characters ' ', 
 * return the length of last word in the string.
 If the last word does not exist, return 0.
 Note: A word is defined as a character sequence consists of non-space characters only.
 For example,
 Given s = "Hello World",
 return 5.*/
public class LengthOfLastWord {

	public int lengthOfLastWord(String s) {
		int lenLastW = 0, len = s.length(), i = 0;
		boolean prevIsSpace = false;
		while (i < len) {
			if (s.charAt(i) != ' ') {
				if (prevIsSpace)
					lenLastW = 1;
				else
					lenLastW++;
				prevIsSpace = false;
			} else
				prevIsSpace = true;
			i++;
		}
		return lenLastW;
	}
}
