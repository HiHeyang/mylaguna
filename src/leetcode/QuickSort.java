package leetcode;

public class QuickSort {

	public void Quicksort(int a[], int start, int end) {
		int i, j;
		i = start;
		j = end;
		if (a == null || a.length == 0)
			return;
		while (i < j) {
			while (i < j && a[i] <= a[j])
				j--;
			if (i < j) {
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
			while (i < j && a[i] < a[j])
				i++;
			if (i < j) {
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
		if (i - start > 1)
			Quicksort(a, start, i - 1);
		if (end - j > 1)
			Quicksort(a, j + 1, end);
	}
}
