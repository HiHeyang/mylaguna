package leetcode;

/*Given a digit string, return all possible letter combinations that the number could represent.
 A mapping of digit to letters (just like on the telephone buttons) is given below.
 Input:Digit string "23"
 Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].*/
import java.util.ArrayList;
import java.util.List;

public class LetterCombinationsOfAPhoneNumber {

	public List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<String>();
		if (digits == null)
			return result;
		String[] keyboard = { "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv",
				"wxyz" };
		StringBuilder current = new StringBuilder();
		buildResult(digits, 0, current, keyboard, result);
		return result;
    }
    private void buildResult(String digits, int index, StringBuilder current,
			String[] keyboard, List<String> result) {
		if (index == digits.length()) {
			result.add(current.toString());
			return;
		}
		int num = digits.charAt(index) - '2';
		for (int i = 0; i < keyboard[num].length(); i++) {
			current.append(keyboard[num].charAt(i));
			buildResult(digits, index + 1, current, keyboard, result);
			current.deleteCharAt(current.length() - 1);
		}
	}
}
