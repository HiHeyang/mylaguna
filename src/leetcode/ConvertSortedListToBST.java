package leetcode;

/*Given a singly linked list where elements are sorted in ascending order, 
 *convert it to a height balanced BST.*/
public class ConvertSortedListToBST {

	public TreeNode sortedListToBST(ListNode head) {
		if (head == null)
			return null;
		ListNode slow = head, fast = head;
		ListNode dummy = new ListNode(0);
		dummy.next = head;
		while (fast != null && fast.next != null) {
			fast = fast.next.next;
			slow = slow.next;
			dummy = dummy.next;
		}
		TreeNode root = new TreeNode(slow.val);
		dummy.next = null;
		if (slow == head)
			head = null;
		root.left = sortedListToBST(head);
		root.right = sortedListToBST(slow.next);
		return root;
	}
}
