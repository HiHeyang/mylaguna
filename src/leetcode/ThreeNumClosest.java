package leetcode;

/*Given an array S of n integers, find three integers in S such that the sum is closest to a given number, 
 target. Return the sum of the three integers. You may assume that each input would have exactly one solution.*/

public class ThreeNumClosest {

	public int threenumclosest(int[] num, int target) {
		if (num == null || num.length == 0)
			return -1;
		if (num.length <= 3) {
			int sum = 0;
			for (int n : num)
				sum += n;
			return sum;
		}
		java.util.Arrays.sort(num);
		int result = num[0] + num[1] + num[2];
		for (int i = 0; i < num.length - 2; i++) {
			int begin = i + 1;
			int end = num.length - 1;
			while (end > begin) {
				int sum = num[i] + num[begin] + num[end];
				if (sum == target)
					return sum;
				if (Math.abs(sum - target) < Math.abs(result - target))
					result = sum;
				if (sum < target)
					begin++;
				else
					end--;
			}
		}
		return result;
	}
}
