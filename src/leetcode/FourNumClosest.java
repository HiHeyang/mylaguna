package leetcode;

/*Given an array S of n integers, are there elements a, b, c, and d in S such that a + b + c + d = target?
 Find all unique quadruplets in the array which gives the sum of target.
 time complexity: best,average,worse O(n^3)
 this uses a hashset to deduplicate.
 this one seldom exceeds time limit. better in term of program runtime.*/

import java.util.ArrayList;
import java.util.HashSet;

public class FourNumClosest {

	public ArrayList<ArrayList<Integer>> fourSum(int[] num, int target) {
		if (num == null || num.length < 4)
			return new ArrayList<ArrayList<Integer>>();
		java.util.Arrays.sort(num);
		int len1 = num.length - 3;
		int len2 = num.length - 2;
		HashSet<ArrayList<Integer>> set = new HashSet<ArrayList<Integer>>();
		for (int i = 0; i < len1; i++) {
			for (int j = (i + 1); j < len2; j++) {
				int first2sum = num[i] + num[j];
				int begin = j + 1;
				int end = num.length - 1;
				while (end > begin) {
					int sum = first2sum + num[begin] + num[end];
					if (sum == target) {
						ArrayList<Integer> list = new ArrayList<Integer>();
						list.add(num[i]);
						list.add(num[j]);
						list.add(num[begin]);
						list.add(num[end]);
						set.add(list);
						begin++;
						end--;
					} else if (sum < target)
						begin++;
					else
						end--;
				}
			}
		}
		return new ArrayList<ArrayList<Integer>>(set);
	}
}
