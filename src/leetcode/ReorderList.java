package leetcode;

/*Given a singly linked list L: L0→L1→…→Ln-1→Ln,
 reorder it to: L0→Ln→L1→Ln-1→L2→Ln-2→…
 You must do this in-place without altering the nodes' values.
 For example,
 Given {1,2,3,4}, reorder it to {1,4,2,3}.*/
public class ReorderList {

	public void reorderList(ListNode head) {
		if (head == null || head.next == null)
			return;
		ListNode slowNode = head, fastNode = head;
		while (fastNode.next != null && fastNode.next.next != null) {
			fastNode = fastNode.next.next;
			slowNode = slowNode.next;
		}
		ListNode head1 = head, head2 = slowNode.next;
		slowNode.next = null;
		ListNode cur = head2, post = cur.next;
		cur.next = null;
		while (post != null) {
			ListNode next = post.next;
			post.next = cur;
			cur = post;
			post = next;
		}
		head2 = cur;
		ListNode p = head1, q = head2;
		while (q != null) {
			ListNode temp1 = p.next;
			ListNode temp2 = q.next;
			p.next = q;
			q.next = temp1;
			p = temp1;
			q = temp2;
		}
	}
}
