package leetcode;

/*Write a program to solve a Sudoku puzzle by filling the empty cells.
 Empty cells are indicated by the character '.'.
 You may assume that there will be only one unique solution.
 A sudoku puzzle...
 (row and column should be 1-9 uniquely)
 ...and its solution numbers marked in red.*/
import java.util.ArrayList;

public class SudokuSolver {

	public void solveSudoku(char[][] board) {
		ArrayList<Integer> array = getArrayList(board);
		DFS(board, array, 0);
	}

	public ArrayList<Integer> getArrayList(char[][] board) {
		ArrayList<Integer> array = new ArrayList<Integer>();
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
				if (board[i][j] == '.')
					array.add(i * 9 + j);
			}
		}
		return array;
	}

	public boolean DFS(char[][] board, ArrayList<Integer> array, int index) {
		int len = array.size();
		if (index == len)
			return true;
		int position = array.get(index);
		int row = position / 9;
		int column = position % 9;
		for (int i = 1; i <= 9; i++) {
			if (isValid(board, row, column, i)) {
				board[row][column] = (char) (i + '0');
				if (DFS(board, array, index + 1))
					return true;
				board[row][column] = '.';
			}
		}
		return false;
	}

	public boolean isValid(char[][] board, int row, int column, int num) {
		for (int i = 0; i < 9; i++) {
			if (board[row][i] - '0' == num)
				return false;
			if (board[i][column] - '0' == num)
				return false;
			int row_s = 3 * (row / 3) + i / 3;
			int column_s = 3 * (column / 3) + i % 3;
			if (board[row_s][column_s] - '0' == num)
				return false;
		}
		return true;
	}
}
