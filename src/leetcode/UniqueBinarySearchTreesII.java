package leetcode;

/*Given n, generate all structurally unique BST's (binary search trees) that store values 1...n.

 For example,
 Given n = 3, your program should return all 5 unique BST's shown below.

 1         3     3      2      1
 \       /     /      / \      \
  3     2     1      1   3      2
 /     /       \                 \
 2     1         2                 3*/
import java.util.ArrayList;

public class UniqueBinarySearchTreesII {

	public ArrayList<TreeNode> generateTrees(int n) {
		return helper(1, n);
	}

	public ArrayList<TreeNode> helper(int left, int right) {
		ArrayList<TreeNode> rs = new ArrayList<TreeNode>();
		if (left > right)
			rs.add(null);
		else if (left == right)
			rs.add(new TreeNode(left));
		else {
			for (int i = left; i <= right; i++) {
				ArrayList<TreeNode> leftNodeSet = helper(left, i - 1);
				ArrayList<TreeNode> rightNodeSet = helper(i + 1, right);
				for (int j = 0; j < leftNodeSet.size(); j++) {
					for (int k = 0; k < rightNodeSet.size(); k++) {
						TreeNode curr = new TreeNode(i);
						curr.left = leftNodeSet.get(j);
						curr.right = rightNodeSet.get(k);
						rs.add(curr);
					}
				}
			}
		}
		return rs;
	}
}
