package leetcode;

/*A message containing letters from A-Z is being encoded to numbers using the following mapping:
 'A' -> 1
 'B' -> 2
 ...
 'Z' -> 26
 Given an encoded message containing digits, determine the total number of ways to decode it.
 For example, Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).
 The number of ways decoding "12" is 2.
 Time is O(n) and space is O(1). */
public class DecodeWays {
	
	// iteration
	public int numDecodings(String s) {
		if (s == null || s.length() == 0)
			return 0;
		int prev = 1, prevprev = 0;
		for (int i = 0; i < s.length(); i++) {
			int temp = 0;
			if (s.charAt(i) != '0')
				temp += prev;
			if (i > 0 && isWith10To26(s, i - 1))
				temp += prevprev;
			prevprev = prev;
			prev = temp;
		}
		return prev;
	}
	
	public boolean isWith10To26(String s, int x) {
		int a = Integer.parseInt(s.substring(x, x + 2));
		return s.charAt(x) != '0' && a <= 26 && a >= 10;
	}
	
	// recursion
	public int numDecodingsRec(String s) {
		if (s == null || s.length() == 0)
			return 0;
		return numDecodings(s, 0);
	}

	public int numDecodings(String s, int start) {
		int last = s.length() - 1;
		if (start == last)
			return s.charAt(start) == '0' ? 0 : 1;
		if (start > last)
			return 1;
		int res = 0;
		if (s.charAt(start) != '0')
			res += numDecodings(s, start + 1);
		if (isWith10To26(s, start))
			res += numDecodings(s, start + 2);
		return res;
	}
}
