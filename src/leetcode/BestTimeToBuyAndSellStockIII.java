package leetcode;

/*Say you have an array for which the ith element is the price of a given stock on day i.
 Design an algorithm to find the maximum profit. You may complete at most two transactions.
 Note:
 You may not engage in multiple transactions at the same time 
 (ie, you must sell the stock before you buy again).*/
public class BestTimeToBuyAndSellStockIII {

	public int maxProfit(int[] prices) {
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
		int len = prices.length;
		int[] forward = new int[len], backward = new int[len];
		for (int i = 0; i < prices.length; i++) {
			if (prices[i] < min)
				min = prices[i];
			if (i > 0) {
				if (prices[i] - min > forward[i - 1])
					forward[i] = prices[i] - min;
				else
					forward[i] = forward[i - 1];
			}
			
			if (prices[len - 1 - i] > max)
				max = prices[len - 1 - i];
			if (i > 0) {
				if (max - prices[len - 1 - i] > backward[len - i])
					backward[len - 1 - i] = max - prices[len - 1 - i];
				else
					backward[len - 1 - i] = backward[len - i];
			}			
		}
		int res = 0;
		for (int i = 0; i < len; i++)
			res = Math.max(forward[i] + backward[i], res);
		return res;
	}
}
