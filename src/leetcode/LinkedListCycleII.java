package leetcode;

/*Given a linked list, return the node where the cycle begins. If there is no cycle, return null.
 Follow up:
 Can you solve it without using extra space?*/

public class LinkedListCycleII {

	public ListNode detectCycle(ListNode head) {
		if (head == null || head.next == null)
			return null;
		ListNode slow = head;
		ListNode fast = head;
		ListNode third = head;
		boolean hascycle = false;
		while (fast != null && fast.next != null) {
			slow = slow.next;
			fast = fast.next.next;
			if (slow == fast) {
				hascycle = true;
				break;
			}
		}
		if (hascycle) {
			while (slow.next != null && third.next != null) {
				if (slow == third)
					break;
				slow = slow.next;
				third = third.next;
			}
			return slow;
		}
		return null;
	}
}
