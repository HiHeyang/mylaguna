package leetcode;

/*Given a binary tree and a sum, find all root-to-leaf paths where each path's sum equals the given sum.
 For example: Given the below binary tree and sum = 22,
 5
 / \
 4   8
 /   / \
 11  13  4
 /  \    / \
 7    2  5   1
 return
 [
 [5,4,11,2],
 [5,8,4,5]
 ]
 * Idea:
 * DFS like recursion. If root is null, then it returns empty.
 * Note when to add and when to remove node.*/
import java.util.ArrayList;
import java.util.List;

public class PathSumII {

	public List<List<Integer>> pathSum(TreeNode root, int sum) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		ArrayList<Integer> path = new ArrayList<Integer>();
		if (root == null)
			return ret;
		dfs(root, sum, path, ret, 0);
		return ret;
	}

	public void dfs(TreeNode root, int sum, ArrayList<Integer> path,
			List<List<Integer>> ret, int total) {
		path.add(root.val);
		if (root.left == null && root.right == null && root.val + total == sum) {
			ArrayList<Integer> tempPath = new ArrayList<Integer>(path);
			ret.add(tempPath);
			return;
		}
		if (root.left != null) {
			dfs(root.left, sum, path, ret, root.val + total);
			path.remove(path.size() - 1);
		}
		if (root.right != null) {
			dfs(root.right, sum, path, ret, root.val + total);
			path.remove(path.size() - 1);
		}
	}
}
