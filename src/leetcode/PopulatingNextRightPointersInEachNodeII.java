package leetcode;

/*Follow up for problem "Populating Next Right Pointers in Each Node".
 What if the given tree could be any binary tree? Would your previous solution still work?
 Note:
 You may only use constant extra space.
 For example, Given the following binary tree,
 1
 /  \
 2    3
 / \    \
 4   5    7
 After calling your function, the tree should look like:
 1 -> NULL
 /  \
 2 -> 3 -> NULL
 / \    \
 4-> 5 -> 7 -> NULL*/
import java.util.ArrayList;

import java.util.List;

public class PopulatingNextRightPointersInEachNodeII {

	public void connect(TreeLinkNode root) {
		if (root == null)
			return;
		TreeLinkNode cur = null;
		List<TreeLinkNode> nextLevelNodes = new ArrayList<TreeLinkNode>();
		nextLevelNodes.add(root);
		while (nextLevelNodes.size()>0) {
			cur = nextLevelNodes.get(0);
			nextLevelNodes.clear();
			while (cur != null) {
				if (cur.left != null)
					nextLevelNodes.add(cur.left);
				if (cur.right != null)
					nextLevelNodes.add(cur.right);
				cur = cur.next;
			}
			for (int i = 0; i < nextLevelNodes.size() - 1; i++)
				nextLevelNodes.get(i).next = nextLevelNodes.get(i + 1);
		}
	}
}
