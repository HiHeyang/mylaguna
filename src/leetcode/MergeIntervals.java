package leetcode;

/*Given a collection of intervals, merge all overlapping intervals.
 For example,
 Given [1,3],[2,6],[8,10],[15,18],
 return [1,6],[8,10],[15,18].
 Data structure: stack. Sort intervals by start time. Push intervals[0] to
 stack firstly. If no overlap, push it. If overlap and current end > prev.end,
 update end of stack top. Finally, use while loop to dump stack to arraylist.
 Time complexity: O(nlogn)
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MergeIntervals {

	public ArrayList<Interval> merge(ArrayList<Interval> intervals) {
		if (intervals.size() == 0)
			return new ArrayList<Interval>();
		Collections.sort(intervals, new Comparator<Interval>() {
			public int compare(Interval arg0, Interval arg1) {
				return arg0.start - arg1.start;
			}
		});
		ArrayList<Interval> res = new ArrayList<Interval>();
        int len = intervals.size();
        Interval prev = intervals.get(0);
        for (int i = 1; i < len; i++) {
            Interval curr = intervals.get(i);
            if (prev.end >= curr.start) {
                prev = new Interval(Math.min(prev.start, curr.start), Math.max(
                        prev.end, curr.end));
            } else {
                res.add(prev);
                prev = curr;
            }
        }
        res.add(prev);
        return res;
	}
}
