package leetcode;

/*Given a binary tree
 Populate each next pointer to point to its next right node. If there is no next right node, the next pointer should be set toNULL.
 Initially, all next pointers are set to NULL.
 Note:
 You may only use constant extra space.
 You may assume that it is a perfect binary tree (ie, all leaves are at the same level, and every parent has two children).
 For example,
 Given the following perfect binary tree,
 1
 /  \
 2    3
 / \  / \
 4  5  6  7
 After calling your function, the tree should look like:
 1 -> NULL
 /  \
 2 -> 3 -> NULL
 / \  / \
 4->5->6->7 -> NULL*/
public class PopulatingNextRightPointersInEachNode {

	public void connect(TreeLinkNode root) {
		if (root == null)
			return;
		TreeLinkNode cur = null, top = root, nextLevelStart = root.left;
		while (top != null && top.left != null) {
			while (top != null) {
				cur = top.left;
				cur.next = top.right;
				cur = cur.next;
				top = top.next;
				cur.next = (top == null) ? null : top.left;
			}
			top = nextLevelStart;
			nextLevelStart = (top == null) ? null : top.left;
		}
	}
}
