package leetcode;

//Sort a linked list using insertion sort.
public class InsertionSortList {

	public ListNode insertionSortList(ListNode head) {
		if (head == null || head.next == null)
			return head;
		ListNode preHead = new ListNode(-1);
		preHead.next = head;
		ListNode cur = head;
		while (cur != null && cur.next != null) {
			if (cur.val > cur.next.val) {
				ListNode smallNode = cur.next;
				ListNode pre = preHead;
				while (pre.next.val < smallNode.val) {
					pre = pre.next;
				}
				ListNode temp = pre.next;
				pre.next = smallNode;
				cur.next = smallNode.next;
				smallNode.next = temp;
			} else
				cur = cur.next;
		}
		return preHead.next;
	}
}
