package leetcode;

/*Given a set of non-overlapping intervals, insert a new interval into the intervals 
 (merge if necessary).
 You may assume that the intervals were initially sorted according to their start times.
 Example 1:
 Given intervals [1,3],[6,9], insert and merge [2,5] in as [1,5],[6,9].
 Example 2:
 Given [1,2],[3,5],[6,7],[8,10],[12,16], insert and merge [4,9] in as [1,2],[3,10],[12,16].
 This is because the new interval [4,9] overlaps with [3,5],[6,7],[8,10].
 */
import java.util.ArrayList;

public class InsertInterval {

	public ArrayList<Interval> insert(ArrayList<Interval> intervals,
			Interval newInterval) {
		int size = intervals.size();
		boolean added = false;
		ArrayList<Interval> resultList = new ArrayList<Interval>();
		if (size == 0) {
			resultList.add(newInterval);
			added = true;
		}
		for (int i = 0; i < size; i++) {
			int start = intervals.get(i).start;
			int end = intervals.get(i).end;
			if (start > newInterval.end) {
				resultList.add(newInterval);
				added = true;
				while (i < size)
					resultList.add(intervals.get(i++));
			} else if (end < newInterval.start)
				resultList.add(intervals.get(i));
			else {
				newInterval.start = Math.min(newInterval.start, start);
				newInterval.end = Math.max(newInterval.end, end);
			}
		}
		if (!added)
			resultList.add(newInterval);
		return resultList;
	}
}
