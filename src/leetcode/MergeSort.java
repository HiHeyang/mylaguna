package leetcode;

public class MergeSort {
	
	public void mergeSort(int[] A, int p, int r) {
		if (p < r) {
			int mid = p + (r - p) / 2;
			mergeSort(A, p, mid);
			mergeSort(A, mid + 1, r);
			merge(A, p, mid, r);
		}
	}

	// merge A[p.. mid], A[mid+1...r] into A[p...r]
	public void merge(int A[], int first, int mid, int last) {
		int left = first, right = mid + 1;
		if (A[mid] <= A[right])
			return;
		while (left <= mid && right <= last) {
			if (A[left] <= A[right])
				left++;
			else {
				int tmp = A[right];
				System.arraycopy(A, left, A, left + 1, right - left);
				A[left] = tmp;
				left++;
				mid++;
				right++;
			}
		}
	}
}
