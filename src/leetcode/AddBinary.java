package leetcode;

/*Given two binary strings, return their sum (also a binary string).
 For example,
 a = "11"
 b = "1"
 Return "100".*/
public class AddBinary {

	// recursive version
	public String addBinaryRec(String a, String b) {
		return addBinaryRecursive(a, b, 0);
	}

	public String addBinaryRecursive(String a, String b, int carry) {
		if (a == null && b == null)
			return carry == 1 ? "1" : null;
		int val = carry;
		if (a != null)
			val += a.charAt(a.length() - 1) - '0';
		if (b != null)
			val += b.charAt(b.length() - 1) - '0';
		int result = val % 2;
		String before = addBinaryRecursive(
				a != null && a.length() > 1 ? a.substring(0, a.length() - 1)
						: null,
				b != null && b.length() > 1 ? b.substring(0, b.length() - 1)
						: null, val >= 2 ? 1 : 0);
		return (before == null ? "" : before) + result;
	}

	// iterative version
	public String addBinary(String a, String b) {
		int carry = 0;
		StringBuilder sb = new StringBuilder();
		String aa, bb;
		if (a.length() < b.length()) {
			aa = a;
			bb = b;
		} else {
			bb = a;
			aa = b;
		}
		int n = bb.length() - aa.length();
		while (n > 0) {
			aa = "0" + aa;
			n--;
		}
		for (int i = 0; i < aa.length(); i++) {
			int total = (aa.charAt(aa.length() - 1 - i) - '0')
					+ (bb.charAt(bb.length() - 1 - i) - '0') + carry;
			carry = total / 2;
			sb.append(total % 2);
		}
		if (carry == 1)
			sb.append("1");
		return sb.reverse().toString();
	}
}
