package leetcode;
/* N-Queens
The n-queens puzzle is the problem of placing n queens on an n^n chessboard 
such that no two queens attack each other.
Given an integer n, return all distinct solutions to the n-queens puzzle.*/
import java.util.ArrayList;

public class NQueens {

	public ArrayList<String[]> solveNQueens(int n) {
		ArrayList<String[]> res = new ArrayList<String[]>();
		int[] row = new int[n];
		dfs(res, row, 0, n);
		return res;
	}

	public void dfs(ArrayList<String[]> res, int[] row, int index, int n) {
		if (index == n)
			printboard(res, row, n);
		else {
			for (int i = 0; i < n; i++) {
				row[index] = i;
				if (isValid(row, index))
					dfs(res, row, index + 1, n);
			}
		}
	}

	public boolean isValid(int[] row, int index) {
		for (int i = 0; i < index; i++) {
			if (row[i] == row[index]
					|| Math.abs(row[i] - row[index]) == (index - i))
				return false;
		}
		return true;
	}

	public void printboard(ArrayList<String[]> res, int[] loc, int n) {
		String[] ans = new String[n];
		for (int i = 0; i < n; i++) {
			String row = new String();
			for (int j = 0; j < n; j++) {
				if (j == loc[i])
					row += "Q";
				else
					row += ".";
			}
			ans[i] = row;
		}
		res.add(ans);
	}
}
