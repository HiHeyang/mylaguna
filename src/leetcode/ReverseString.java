package leetcode;

/*Given an input string, reverse the string word by word.
 For example,
 Given s = "the sky is blue",
 return "blue is sky the".*/

public class ReverseString {

	public String reverseWords(String s) {
		if (s == null || s.length() == 0)
			return new String("");
		if (s.length() == 1 && !s.equals(" "))
			return s;
		s = s.replaceAll(" +", " ").trim();
		char[] str = s.toCharArray();
		int start = 0, end = 0;
		reverseWord(str, start, str.length - 1);
		while (end < str.length) {
			if (str[end] != ' ') {
				start = end;
				while (end < str.length && str[end] != ' ')
					end++;
				end--;
				reverseWord(str, start, end);
			}
			end++;
		}
		s = String.valueOf(str);
		return s;
	}

	public void reverseWord(char[] str, int start, int end) {
		while (start < end) {
			char temp = str[start];
			str[start] = str[end];
			str[end] = temp;
			start++;
			end--;
		}
	}
}
