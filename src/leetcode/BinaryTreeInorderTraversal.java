package leetcode;

/*
 *     1
 *    / \
 *   2   3
 *  / \   \
 * 4   5   6  
 *          \
 *           7
 *            
 * in order: 4, 2, 5, 1, 3, 6, 7
 * */
import java.util.ArrayList;
import java.util.Stack;

public class BinaryTreeInorderTraversal {

	// recursion
	public ArrayList<Integer> inorderTraversal_Recursion(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		inorder(res, root);
		return res;
	}

	public void inorder(ArrayList<Integer> res, TreeNode root) {
		if (root.left != null)
			inorder(res, root.left);
		res.add(root.val);
		if (root.right != null)
			inorder(res, root.right);
	}

	// Iteration
	public ArrayList<Integer> inorderTraversal_Iteration(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		Stack<TreeNode> s = new Stack<TreeNode>();
		TreeNode cur = root;
		while (!s.isEmpty() || cur != null) {
			if (cur != null) {
				s.push(cur);
				cur = cur.left;
			} else {
				cur = s.pop();
				res.add(cur.val);
				cur = cur.right;
			}
		}
		return res;
	}
}
