package leetcode;

/*A linked list is given such that each node contains an additional random pointer 
 * which could point to any node in the list or null.
 Return a deep copy of the list.*/
import java.util.HashMap;

public class CopyListWithRandomPointer {

	public RandomListNode copyRandomList(RandomListNode head) {
		if (head == null)
			return null;
		return copy(head);
	}

	private RandomListNode copy(RandomListNode node) {
		HashMap<RandomListNode, RandomListNode> hm = new HashMap<RandomListNode, RandomListNode>();
		RandomListNode p = node;
		while (p != null) {
			hm.put(p, new RandomListNode(p.label));
			p = p.next;
		}
		p = node;
		RandomListNode head = null;
		RandomListNode current = null;
		while (p != null) {
			if (head == null) {
				head = hm.get(p);
				current = head;
			} else {
				current.next = hm.get(p);
				current = current.next;
			}
			if (p.random != null)
				current.random = hm.get(p.random);
			p = p.next;
		}
		return head;
	}
}
