package leetcode;

/*Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
 For example,
 If n = 4 and k = 2, a solution is:
 [
 [2,4],
 [3,4],
 [2,3],
 [1,2],
 [1,3],
 [1,4],
 ]*/
import java.util.ArrayList;
import java.util.List;

public class Combinations {

	public List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<List<Integer>>();
		combinations(new ArrayList<Integer>(), 1, n, k, result);
		return result;
    }
	
    public void combinations(ArrayList<Integer> sofar, int start, int n, int k,
			List<List<Integer>> result) {
		if (sofar.size() == k) {
		    ArrayList<Integer> temp = new ArrayList<Integer>(sofar);
			result.add(temp);
			return;
		}
		for (int i = start; i <= n; i++) {
			sofar.add(i);
			combinations(sofar, i + 1, n, k, result);
			sofar.remove(sofar.size()-1);
		}
	}
}