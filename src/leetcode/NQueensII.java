package leetcode;

/*Follow up for N-Queens problem.
 Now, instead outputting board configurations, return the total number of distinct solutions.
 */
public class NQueensII {
	int[] colForRow;
	int cnt = 0;

	public int totalNQueens(int n) {
		colForRow = new int[n];
		dfs(0, n);
		return cnt;
	}

	private void dfs(int index, int n) {
		if (index == n)
			cnt++;
		else {
			for (int i = 0; i < n; i++) {
				colForRow[index] = i;
				if (check(index))
					dfs(index + 1, n);
			}
		}
	}

	private boolean check(int index) {
		for (int i = 0; i < index; i++) {
			int diff = Math.abs(colForRow[i] - colForRow[index]);
			if (diff == 0 || diff == index - i)
				return false;
		}
		return true;
	}
}
