package leetcode;

/*Given a list, rotate the list to the right by k places, where k is non-negative.
 For example:
 Given 1->2->3->4->5->NULL and k = 2,
 return 4->5->1->2->3->NULL.*/
public class RotateList {

	public ListNode rotateList(ListNode head, int k) {
		if (head == null)
			return null;
		if (head.next == null || k == 0)
			return head;
		ListNode now = head;
		for (int i = 0; i < k - 1; i++) {
			if (now.next == null)
				now = head;
			else
				now = now.next;
		}
		ListNode kth = now;
		ListNode pre = head;
		now = head;
		if (kth.next == null)
			return head;
		else {
			while (kth.next != null) {
				kth = kth.next;
				pre = now;
				now = now.next;
			}
			pre.next = null;
			kth.next = head;
			return now;
		}
	}
}
