package leetcode;

/*Given a collection of candidate numbers (C) and a target number (T), 
 * find all unique combinations in C where the candidate numbers sums to T.
 Each number in C may only be used once in the combination.
 Note:
 All numbers (including target) will be positive integers.
 Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
 The solution set must not contain duplicate combinations.
 For example, given candidate set 10,1,2,7,6,1,5 and target 8
 A solution set is:  [1, 7]  [1, 2, 5]  [2, 6]  [1, 1, 6] */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSumII {

	public List<List<Integer>> combinationSum2(int[] num, int target) {
        List<List<Integer>> list = new ArrayList<List<Integer>>();
		Arrays.sort(num);
		combinationSum(num, target, 0, new ArrayList<Integer>(), list);
		return list;
    }
	
    public void combinationSum(int[] num, int target, int start,
			ArrayList<Integer> sofar, List<List<Integer>> list) {
		if (target == 0) {
		    ArrayList<Integer> temp = new ArrayList<Integer>(sofar);
			list.add(temp);
			return;
		}
		for (int i = start; i < num.length; i++) {
			if (num[i] > target)
				break;
			sofar.add(num[i]);
			combinationSum(num, target - num[i], i + 1, sofar, list);
			sofar.remove(sofar.size()-1);
			while (i < num.length - 1
					&& num[i] == num[i + 1])
				i++;
		}
	}
}
