package leetcode;

/* Given an array of strings, return all groups of strings that are anagrams.
 Note: All inputs will be in lower-case. */
import java.util.*;
 
public class Anagrams {

	public List<String> anagrams(String[] strs) {
		int len = strs.length;
		List<String> result = new ArrayList<String>();
		HashMap<String, Integer> hash = new HashMap<String, Integer>();
		if (len == 0)
			return result;
		for (int i = 0; i < len; i++) {
			char[] array = strs[i].toCharArray();
			Arrays.sort(array);
		    String temp = String.valueOf(array);
			if (hash.containsKey(temp))
				hash.put(temp, hash.get(temp) + 1);
			else
				hash.put(temp, 1);
		}
		for (int i = 0; i < len; i++) {
			char[] array = strs[i].toCharArray();
			Arrays.sort(array);
		    String temp = String.valueOf(array);
			if (hash.containsKey(temp) && hash.get(temp) >= 2)
				result.add(strs[i]);
		}
		return result;
	}
}
