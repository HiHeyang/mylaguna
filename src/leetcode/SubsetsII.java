package leetcode;
/*Given a collection of integers that might contain duplicates, S, return all possible subsets.
Note:
Elements in a subset must be in non-descending order.
The solution set must not contain duplicate subsets.
For example,
If S = [1,2,2], a solution is:
[
[2],
[1],
[1,2,2],
[2,2],
[1,2],
[]
]*/
import java.util.*;

public class SubsetsII {

	public List<List<Integer>> subsetsWithDup(int[] num) {
		if (num == null)
			return null;
		Arrays.sort(num);
		List<List<Integer>> res = new ArrayList<List<Integer>>();
		List<List<Integer>> cur = new ArrayList<List<Integer>>();
		for (int i = 0; i < num.length; i++) {
			if (i == 0 || num[i] != num[i - 1]) {
				cur = new ArrayList<List<Integer>>();
				for (int j = 0; j < res.size(); j++)
					cur.add(new ArrayList<Integer>(res.get(j)));
			}
			for (List<Integer> temp : cur)
				temp.add(num[i]);
			if (i == 0 || num[i] != num[i - 1]) {
				List<Integer> temp1 = new ArrayList<Integer>();
				temp1.add(num[i]);
				cur.add(temp1);
			}
			for (List<Integer> temp : cur)
				res.add(new ArrayList<Integer>(temp));
		}
		res.add(new ArrayList<Integer>());
		return res;
	}
}
