package leetcode;

/*Given a set of candidate numbers (C) and a target number (T), 
 * find all unique combinations in C where the candidate numbers sums to T.
 The same repeated number may be chosen from C unlimited number of times.
 Note:
 All numbers (including target) will be positive integers.
 Elements in a combination (a1, a2, … , ak) must be in non-descending order. (ie, a1 ≤ a2 ≤ … ≤ ak).
 The solution set must not contain duplicate combinations.
 For example, given candidate set 2,3,6,7 and target 7, 
 A solution set is: 
 [7] 
 [2, 2, 3] */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinationSum {

	public List<List<Integer>> combinationSum(int[] candidates, int target) {
		List<List<Integer>> list = new ArrayList<List<Integer>>();
		Arrays.sort(candidates);
		combinationSum(candidates, target, 0, new ArrayList<Integer>(), list);
		return list;
	}

	public void combinationSum(int[] candidates, int target, int start,
			ArrayList<Integer> sofar, List<List<Integer>> list) {
		if (target == 0) {
			ArrayList<Integer> temp = new ArrayList<Integer>(sofar);
			list.add(temp);
			return;
		}
		for (int i = start; i < candidates.length; i++) {
			if (candidates[i] > target)
				break;
			sofar.add(candidates[i]);
			combinationSum(candidates, target - candidates[i], i, sofar, list);
			sofar.remove(sofar.size() - 1);
		}
	}
}
