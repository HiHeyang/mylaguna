package leetcode;

/*Given an array of non-negative integers, you are initially positioned at the first index of the array.
 Each element in the array represents your maximum jump length at that position.
 Determine if you are able to reach the last index.
 For example:
 A = [2,3,1,1,4], return true.
 A = [3,2,1,0,4], return false.
 idea: keep track of farthest reachable position.*/
public class JumpGame {
	public boolean canJump(int[] A) {
		if (A == null || A.length == 0)
			return false;
		int len = A.length;
		if (len == 1)
			return true;
		int max = 0;
		for (int i = 0; i < len; i++) {
			if (i <= max) {
				max = Math.max(max, A[i] + i);
				if (max >= len - 1)
					return true;
			}
		}
		return false;
	}
}
