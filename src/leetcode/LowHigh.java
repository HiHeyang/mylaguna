package leetcode;

/*
 * So the goal is to write a function that takes an input array of integers
 * and returns an array with the same integers sorted so that the "low"
 * integers come before the "medium" integers which come before the high
 * integers. Someone already wrote functions for us that do the following:
 * boolean isLow(int a); boolean isMedium(int a); boolean isHigh(int a);
 * void swap(int a, int b, int[] input); 0-4 : low 5-9 : medium 10 - 13:high 
 * 0, 3, 5, 10, 8, 7, 2, 13, 2 
 * 0, 3, 2, 2, 5, 7, 8, 10, 13
 */
public class LowHigh {

	public int[] sortByGroup(int[] inputs) {
		if (inputs == null || inputs.length < 2)
			return inputs;
		int start = 0;
		int low = -1;
		int high = inputs.length;
		while (start < high) {
			int curr = inputs[start];
			if (isLow(curr)) {
				low++;
				swap(low, start, inputs);
			} else if (isHigh(curr)) {
				high--;
				swap(high, start, inputs);
				continue;
			}
			start++;
		}
		return inputs;
	}

	public void swap(int x, int y, int A[]) {
		int temp = A[x];
		A[x] = A[y];
		A[y] = temp;
	}

	public boolean isLow(int x) {
		return x >= 0 & x <= 4;
	}

	public boolean isHigh(int x) {
		return x >= 10 & x <= 13;
	}

	public static void main(String[] args) {
		LowHigh lh = new LowHigh();
		int[] nums = { 10, 10, 8, 7, 0, 1, 5, 12, 3, 2, 13 };
		lh.sortByGroup(nums);
		for (int i : nums)
			System.out.print(i + "  ");
	}
}
