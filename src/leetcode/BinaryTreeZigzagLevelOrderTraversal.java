package leetcode;

/*Given a binary tree, return the zigzag level order traversal of its nodes' values. 
 (ie, from left to right, then right to left for the next level and alternate between).
 For example:
 Given binary tree {3,9,20,#,#,15,7},
 3
 / \
 9  20
 /  \
 15   7
 return its zigzag level order traversal as:
 [
 [3],
 [20,9],
 [15,7]
 ]*/
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

public class BinaryTreeZigzagLevelOrderTraversal {

	public ArrayList<ArrayList<Integer>> zigzagLevelOrder(TreeNode root) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>();
		if (root == null)
			return list;
		LinkedList<TreeNode> queue = new LinkedList<TreeNode>();
		int currentCnt = 1, nextCnt = 0;
		queue.addFirst(root);
		ArrayList<Integer> level = new ArrayList<Integer>();
		Stack<Integer> stack = new Stack<Integer>();
		boolean fromLeftToRight = true;
		while (!queue.isEmpty()) {
			TreeNode node = queue.removeFirst();
			if (fromLeftToRight)
				level.add(node.val);
			else
				stack.push(node.val);
			currentCnt--;
			if (node.left != null) {
				queue.addLast(node.left);
				nextCnt++;
			}
			if (node.right != null) {
				queue.addLast(node.right);
				nextCnt++;
			}
			if (currentCnt == 0) {
				while (!stack.isEmpty())
					level.add(stack.pop());
				list.add(level);
				level = new ArrayList<Integer>();
				currentCnt = nextCnt;
				nextCnt = 0;
				fromLeftToRight = fromLeftToRight == true ? false : true;
			}
		}
		return list;
	}
}
