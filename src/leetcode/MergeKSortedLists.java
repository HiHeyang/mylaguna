package leetcode;

/*Merge k sorted linked lists and return it as one sorted list. 
 Analyze and describe its complexity.*/
import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MergeKSortedLists {

	class Comp implements Comparator<ListNode> {
		public int compare(ListNode arg0, ListNode arg1) {
			return arg0.val - arg1.val;
		}
	}

	public ListNode mergeKLists(ArrayList<ListNode> lists) {
		if (lists.size() == 0)
			return null;
		PriorityQueue<ListNode> queue = new PriorityQueue<ListNode>(
				lists.size(), new Comp());
		for (ListNode list : lists) {
			if (list != null)
				queue.add(list);
		}
		ListNode head = new ListNode(0), curr = head;
		while (!queue.isEmpty()) {
			curr.next = queue.poll();
			curr = curr.next;
			if (curr.next != null)
				queue.add(curr.next);
		}
		return head.next;
	}

	public ListNode mergeKLists2(ArrayList<ListNode> lists) {
		if (lists == null || lists.size() == 0)
			return null;
		ListNode resultList = lists.get(0);
		for (int i = 1; i < lists.size(); i++) {
			if (lists.get(i) != null)
				resultList = Insert(resultList, lists.get(i));
		}
		return resultList;
	}

	private ListNode Insert(ListNode resultList, ListNode list) {
		if (resultList == null)
			return list;
		if (list == null)
			return resultList;
		ListNode dummy = new ListNode(0);
		ListNode current = dummy;
		while (resultList != null && list != null) {
			if (resultList.val < list.val) {
				current.next = resultList;
				current = current.next;
				resultList = resultList.next;
			} else {
				current.next = list;
				current = current.next;
				list = list.next;
			}
		}
		current.next = (resultList == null) ? list : resultList;
		return dummy.next;
	}
}
