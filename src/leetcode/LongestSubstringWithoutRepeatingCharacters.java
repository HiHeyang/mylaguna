package leetcode;

/*Given a string, find the length of the longest substring without repeating characters.
 *For example, the longest substring without repeating letters for "abcabcbb" is "abc", 
 *which the length is 3. For "bbbbb" the longest substring is "b", with the length of 1*/
/*
 * Data structure: boolean[256] to keep chars seen (assuming ASCII). 
 * Keep start and end indices of current substring. Update start when see duplicates.
 * Time complexity: O(n)
 * */
public class LongestSubstringWithoutRepeatingCharacters {

	public int lengthOfLongestSubstring(String s) {
		boolean[] seen = new boolean[256];
		int start = 0, end = 0, size = s.length(), maxLen = 0;
		while (end < size) {
			if (seen[s.charAt(end)]) {
				maxLen = Math.max(maxLen, end - start);
				while (s.charAt(start) != s.charAt(end)) {
					seen[s.charAt(start)] = false;
					start++;
				}
				start++;
				end++;
			} else {
				seen[s.charAt(end)] = true;
				end++;
			}
		}
		maxLen = Math.max(maxLen, size - start);
		return maxLen;
	}
}
