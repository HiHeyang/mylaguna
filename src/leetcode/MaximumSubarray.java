package leetcode;
/*
 Find the contiguous subarray within an array (containing at least one number) 
 which has the largest sum.
 For example, given the array [−2,1,−3,4,−1,2,1,−5,4],
 the contiguous subarray [4,−1,2,1] has the largest sum = 6.
 * Data structure: lastSum.
 * DP. if lastSum > 0, sum[i] = lastSum + A[i] else
 * sum[i] = A[i]. sum[i] represents max sum of subarray ending at A[i].
 * */

public class MaximumSubarray {

	public int maxSubArray(int[] A) {
		int n = A.length;
		if (n == 0)
			return 0;
		int maxSum = A[0], lastSum = A[0];
		for (int i = 1; i < A.length; i++) {
			int sum = A[i];
			if (lastSum > 0)
				sum += lastSum;
			lastSum = sum;
			maxSum = Math.max(maxSum, sum);
		}
		return maxSum;
	}
}