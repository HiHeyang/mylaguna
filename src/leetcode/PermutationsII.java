package leetcode;

/*Given a collection of numbers that might contain duplicates, return all possible unique permutations.
 For example, [1,1,2] have the following unique permutations: [1,1,2], [1,2,1], and [2,1,1].*/
import java.util.ArrayList;

public class PermutationsII {

	public ArrayList<ArrayList<Integer>> permuteUnique(int[] num) {
		ArrayList<ArrayList<Integer>> ret = new ArrayList<ArrayList<Integer>>();
		perm(num, 0, ret);
		return ret;
	}

	public void perm(int[] num, int start, ArrayList<ArrayList<Integer>> ret) {
		if (start == num.length) {
			ArrayList<Integer> item = new ArrayList<Integer>();
			for (int k : num)
				item.add(k);
			ret.add(item);
		}
		for (int i = start; i < num.length; i++) {
			if (shouldSwap(num, start, i)) {
				swap(num, start, i);
				perm(num, start + 1, ret);
				swap(num, start, i);
			}
		}
	}

	public void swap(int[] num, int j, int k) {
		int tmp = num[k];
		num[k] = num[j];
		num[j] = tmp;
	}

	public boolean shouldSwap(int[] num, int m, int n) {
		for (int i = m; i < n; i++)
			if (num[i] == num[n])
				return false;
		return true;
	}
}
