package leetcode;

import java.util.ArrayList;
import java.util.Stack;

public class BinaryTreePostorderTraversal {

	// recursion
	public ArrayList<Integer> postorderTraversal_Recursion(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		post(res, root);
		return res;
	}

	public void post(ArrayList<Integer> res, TreeNode root) {
		if (root.left != null)
			post(res, root.left);
		if (root.right != null)
			post(res, root.right);
		res.add(root.val);
	}

	// iteration
	public ArrayList<Integer> postorderTraversal_Iteration(TreeNode root) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		if (root == null)
			return res;
		Stack<TreeNode> s = new Stack<TreeNode>();
		Stack<Integer> out = new Stack<Integer>();
		TreeNode cur = root;
		while (!s.isEmpty() || cur != null) {
			if (cur != null) {
				out.push(cur.val);
				s.push(cur);
				cur = cur.right;
			} else {
				cur = s.pop();
				cur = cur.left;
			}
		}
		while (!out.isEmpty())
			res.add(out.pop());
		return res;
	}
}
