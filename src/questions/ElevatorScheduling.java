package questions;
/*
 * 一座大厦，里面的电梯一开始在高峰时段每层都停，现设计一种方式，让电梯只停在其中的某一层。所有的乘客从这层爬楼梯到自己的楼层。在一楼的时候，每个乘客选择自己的目的层，电梯自动计算出应该停的层。
问：电梯停在哪一层，能够保证乘坐的所有乘客爬楼梯的层数之和最少。
首先对问题进行抽象->假设楼层共N层，电梯停在第i层，要去第j层的乘客数目总数为nPerson[j]，这样，所爬楼梯的总数就是
Σ{nPerson[j]*|i-j|}，j从1到N*/

public class ElevatorScheduling {
	
	 private static int[] getRes1(int[] nPerson, int N) {
	      int nMinFloor = Integer.MAX_VALUE;
	      int nTargetFloor = 0;//电梯最终要停的目标楼层
	      int nFloor = 0; //如果停在第i层，总共需要爬的楼层数
	      for(int i = 1; i <= N; i++){
	          nFloor = 0;
	          for(int j = 1; j <= N; j++){
	              nFloor += nPerson[j] * Math.abs(i - j);
	          }
	          if(nFloor < nMinFloor){
	              nMinFloor = nFloor;
	              nTargetFloor = i;
	          }
	      }
	      
	      int[] res = new int[2];
	      res[0] = nTargetFloor;
	      res[1] = nMinFloor;
	      return res;
	  }
	 
	 private static int[] getRes2(int[] nPerson, int N) {
	      int nMinFloor = 0;
	      int nTargetFloor = 0;//电梯最终要停的目标楼层
	      int N3 = 0; //N3个乘客在i层以上
	      
	      //求出第一层的层数,以及N3的值
	      for(int i = 2; i <= N; i++){
	          N3 += nPerson[i];
	          nMinFloor += nPerson[i] * Math.abs(i - 1);
	      }
	      
	      //从第2层开始，一直往上找，找出最小
	      int N1 = 0;
	      int N2 = nPerson[1];
	      for(int i = 2; i <= N; i++){
	          if(N1 + N2 < N3){
	              nTargetFloor = i;
	              nMinFloor += N1 + N2 - N3;
	              N1 += N2;
	              N2 = nPerson[i];
	              N3 -= nPerson[i];
	          }else break;
	      }
	      
	      
	      int[] res = new int[2];
	      res[0] = nTargetFloor;
	      res[1] = nMinFloor;
	      return res;
	  }
	 
	 private static int[] getRes3(int[] nPerson, int N) {
	      int left=1,right=N,nFloor = 0;
	      int[] nPersonClone = nPerson.clone();
	      while(right-left>0)//模拟 
	      {
	          while(nPerson[left]==0)
	              left++;
	          nPerson[left]--;
	          while(nPerson[right]==0)
	              right--;
	          nPerson[right]--;
	      }
	      int[] res = new int[2];
	      res[0] = left;
	      
	      //计算第left层时候的要爬的总层数
	      for(int j = 1; j <= N; j++){
	          nFloor += nPersonClone[j] * Math.abs(left - j);
	      }
	      
	      res[1] = nFloor;
	      return res;
	  }
}
