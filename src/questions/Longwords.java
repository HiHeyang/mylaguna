package questions;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class Longwords {

	public String firstLongWord(String[] strs) {
		String str = "";
		Set<String> sets = new HashSet<String>();
		for (int i = 0; i < strs.length; i++)
			sets.add(strs[i]);
		for (int i = 0; i < strs.length; i++) {
			for (int j = 1; j < strs[i].length(); j++)
				if (isCombined(strs[i].substring(0, j), sets)
						&& isCombined(strs[i].substring(j, strs[i].length()),
								sets)) {
					if (strs[i].length() > str.length())
						str = strs[i];
					break;
				}
		}
		return str;
	}

	public String secondLongWord(String[] strs) {
		String[] s = new String[strs.length];
		for (int i = 0; i < s.length; i++) {
			s[i] = strs[i];
			if (s[i].equals(firstLongWord(strs)))
				s[i] = "";
		}
		return firstLongWord(s);
	}

	public int numLongWords(String[] strs) {
		int count = 0;
		Set<String> sets = new HashSet<String>();
		for (int i = 0; i < strs.length; i++)
			sets.add(strs[i]);
		for (int i = 0; i < strs.length; i++) {
			for (int j = 1; j < strs[i].length(); j++)
				if (isCombined(strs[i].substring(0, j), sets)
						&& isCombined(strs[i].substring(j, strs[i].length()),
								sets)) {
					count++;
					break;
				}
		}
		return count;
	}

	public boolean isCombined(String s, Set<String> sets) {
		boolean[] flag = new boolean[s.length() + 1];
		flag[0] = true;
		for (int i = 1; i <= s.length(); i++) {
			for (int j = 0; j < i; j++) {
				if (flag[j] && sets.contains(s.substring(j, i))) {
					flag[i] = true;
					break;
				}
			}
		}
		return flag[s.length()];
	}

	public String[] firstLongWordBruteForce(String filepath) {
		String[] ret = new String[2];
		ret[0] = "";
		ret[1] = "";
		BufferedReader br = null;
		Set<String> dict = new HashSet<String>();
		try {
			br = new BufferedReader(new FileReader(filepath));
			String line = "";
			while ((line = br.readLine()) != null) {
				String word = line.trim();
				if (!word.isEmpty())
					dict.add(word);
			}
		} catch (IOException ex) {
			System.out.println(ex);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (String word : dict) {
			for (int j = 1; j < word.length(); j++) {
				String first = word.substring(0, j);
				String second = word.substring(j, word.length());
				if (isCombined(first, dict) && isCombined(second, dict)) {
					if (word.length() > ret[0].length()) {
						ret[1] = ret[0];
						ret[0] = word;
					}
					break;
				}
			}
		}
		return ret;
	}

	public String[] firstLongWord(String filepath) {
		String[] ret = new String[2];
		ret[0] = "";
		ret[1] = "";
		BufferedReader br = null;
		Set<String> dict = new HashSet<String>();
		Set<String> breakableWords = new HashSet<String>();
		Set<String> breakableParts = new HashSet<String>();

		try {
			br = new BufferedReader(new FileReader(filepath));
			String line = "";
			while ((line = br.readLine()) != null) {
				String word = line.trim();
				if (!word.isEmpty())
					dict.add(word);
			}
		} catch (IOException ex) {
			System.out.println(ex);
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for (String word : dict) {
			for (int j = 1; j < word.length(); j++) {
				String left = word.substring(0, j);
				String right = word.substring(j, word.length());
				boolean leftBreakable = breakableParts.contains(left);
				boolean rightBreakable = breakableParts.contains(right);
				if (!(leftBreakable && rightBreakable)) {
					if (!breakableParts.contains(left)
							&& isCombined(left, dict)) {
						breakableParts.add(left);
						leftBreakable = true;
					}
					if (!breakableParts.contains(right)
							&& isCombined(right, dict)) {
						breakableParts.add(right);
						rightBreakable = true;
					}
				}
				if (leftBreakable && rightBreakable) {
					breakableWords.add(word);
					breakableParts.add(word);
					if (word.length() > ret[0].length()) {
						ret[1] = ret[0];
						ret[0] = word;
					}
					break;
				}
			}
		}
		return ret;
	}

	public static void main(String[] args) {
		// String[] s = { "cat", "cats", "catsdogcats", "catxdogcatsrat", "dog",
		// "dogcatsdog", "hippopotamuses", "rat", "ratcatdogcat" };
		Longwords lw = new Longwords();
		// System.out.println("first longest word is: " + lw.firstLongWord(s));
		// System.out.println("second longest word is: " +
		// lw.secondLongWord(s));
		// System.out.println("count of words is: " + lw.numLongWords(s));

		System.out.println("---------");
		long startTime = System.currentTimeMillis();
		String[] res = lw.firstLongWord("wordsforproblem.txt");
		System.out.println("first longest word is: " + res[0] + "\n"
				+ "second longest word is: " + res[1]);
		long endTime = System.currentTimeMillis();
		System.out.println(endTime - startTime);

		System.out.println("---------");
		startTime = System.currentTimeMillis();
		String[] ress = lw.firstLongWordBruteForce("wordsforproblem.txt");
		System.out.println("first longest word is: " + ress[0] + "\n"
				+ "second longest word is: " + ress[1]);
		endTime = System.currentTimeMillis();
		System.out.println(endTime - startTime);

	}
}
