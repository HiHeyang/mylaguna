package careercup;

public class LinkedListNode {
	int data;
	LinkedListNode next;

	LinkedListNode(int x) {
		data = x;
		next = null;
	}
}
