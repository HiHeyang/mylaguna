package careercup;

public class FindTheMinimumElement {
	public int findtheminimumelement(int[] s) {
		int len = s.length;
		int left = 0, right = len - 1;
		while (left < right) {
			int mid = (left + right) / 2;
			if (s[left] < s[mid])
				left = mid;
			if (s[mid] < s[right])
				right = mid;
			if (left+1==right)
				return s[right];
		}
		return -1;
	}
	public static void main(String[] args){
		int[] s = {4,5,6,1};
		FindTheMinimumElement ft = new FindTheMinimumElement();
		System.out.println(ft.findtheminimumelement(s));
	}
}
