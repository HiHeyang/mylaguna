package careercup;

public class IsRotation {
	public boolean isRotation(String s1, String s2) {
		int len = s1.length();
		if (len == s2.length() && len > 0) {
			String s1s1 = s1 + s1;
			if (s1s1.indexOf(s2) >= 0)
				return true;
		}
		return false;
	}
	public static void main(String[] args){
		IsRotation ir = new IsRotation();
		String st1 = "waterbottle";
		String st2 = "erbottlewat";
		System.out.println(ir.isRotation(st1, st2));
	}
}
