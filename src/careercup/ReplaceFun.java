package careercup;
/*
 * Write a method to replace all spaces in a string with ‘%20’.
 * The algorithm is as follows:
1. Count the number of spaces during the first scan of the string.
2. Parse the string again from the end and for each character:
»If a space is encountered, store “%20”.
»Else, store the character as it is in the newly shifted location.*/
public class ReplaceFun {
	public char[] replacefun(char[] str, int length) {
		int spaceCount = 0, newLength, i = 0;
		for (i = 0; i < length; i++) {
			if (str[i] == ' ') {
				spaceCount++;
			}
		}
		newLength = length + spaceCount * 2;
		char[] s = new char[newLength+1];
		s[newLength] = '\0';
		for (i = length - 1; i >= 0; i--) {
			if (str[i] == ' ') {
				s[newLength - 1] = '0';
				s[newLength - 2] = '2';
				s[newLength - 3] = '%';
				newLength = newLength - 3;
			} else {
				s[newLength - 1] = str[i];
				newLength = newLength - 1;
			}
		}
		return s;
	}
	public static void main(String[] args){
		ReplaceFun rf = new ReplaceFun();
		String s = "riv irk rvd";
		int length = s.length();
		System.out.println(rf.replacefun(s.toCharArray(), length));
	}
}
