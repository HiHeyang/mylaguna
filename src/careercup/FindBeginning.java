package careercup;
/*Given a circular linked list, implement an algorithm which returns node at the beginning of the loop.
DEFINITION
Circular linked list: A (corrupt) linked list in which a node’s next pointer points to an earlier node, so as to make a loop in the linked list.
EXAMPLE
Input: A -> B -> C -> D -> E -> C [the same C as earlier]
Output: C*/
public class FindBeginning {
	public LinkedListNode findBeginning(LinkedListNode head) {
		LinkedListNode n1 = head;
		LinkedListNode n2 = head;
		while (n2.next != null) {
			n1 = n1.next;
			n2 = n2.next.next;
			if (n1 == n2) {
				break;
			}
		}
		if (n2.next == null) {
			return null;
		}
		n1 = head;
		while (n1 != n2) {
			n1 = n1.next;
			n2 = n2.next;
		}
		return n2;
	}
	public static void main(String[] args){
		LinkedListNode one = new LinkedListNode(1);
		LinkedListNode two = new LinkedListNode(2);
		LinkedListNode three = new LinkedListNode(3);
		LinkedListNode four = new LinkedListNode(4);
		LinkedListNode five = new LinkedListNode(5);
		LinkedListNode six = new LinkedListNode(3);
		one.next = two;
		two.next = three;
		three.next = four;
		four.next = five;
		five.next = six;
		FindBeginning fb = new FindBeginning();
		System.out.println(fb.findBeginning(one).data);
	}
}
