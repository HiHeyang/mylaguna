package careercup;
/*Write code to remove duplicates from an unsorted linked list.*/
public class RemoveDuplicatesFromAnUnsortedLinkedList {
	public void deleteDups(LinkedListNode head) {
		if (head == null)
			return;
		LinkedListNode previous = head;
		LinkedListNode current = previous.next;
		while (current != null) {
			LinkedListNode runner = head;
			while (runner != current) {
				if (runner.data == current.data) {
					LinkedListNode tmp = current.next;
					previous.next = tmp;
					current = tmp;
					break;
				}
				runner = runner.next;
			}
			if (runner == current) {
				previous = current;
				current = current.next;
			}
		}
	}
}
